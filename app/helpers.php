<?php

declare(strict_types = 1);

/**
 * @param string $route
 * @param $roles
 * @return bool
 */
function canAccessRoute(string $route, $roles): bool
{
    foreach ($roles as $role) {
        if ($role->full_access) {
            return true;
        }
        foreach ($role->permissions as $permission) {
            if ($permission->route == $route || $permission->name == $route) {
                return true;
            }
        }
    }

    return false;
}

/**
 * @param array $routes
 * @param $roles
 * @return bool
 */
function canAccessAnyRoute(array $routes, $roles): bool
{
    foreach ($roles as $role) {
        if ($role->full_access) {
            return true;
        }

        foreach ($role->permissions as $permission) {
            if (in_array($permission->route, $routes)) {
                return true;
            }
        }
    }

    return false;
}

/**
 * @param string $uri
 * @return string
 */
function addClassOnActivePage(string $uri)
{
    if (Route::getCurrentRoute()->uri() == $uri) {
        return 'menu-is-active';
    }
}