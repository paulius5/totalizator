<?php

namespace App\Providers;

use App\Repositories\BetRepository;
use Auth;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        view()->composer('layouts/app', function ($view){
            $roles = collect();
            if (auth()->check()) {
                $roles = Auth::user()->roles()->where('active', 1)->with('permissions')->get();
            }
            $view->with('userRoles', $roles);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerRepositories();
    }

    /**
     *
     */
    private function registerRepositories(): void
    {
        $this->app->singleton(BetRepository::class);
        //TODO: uzregistruoti visas repozitorijas bei servisus
    }
}
