<?php

declare (strict_types=1);

namespace App\Http\Requests;

use Illuminate\Validation\Rule;

class CategoryUpdateRequest extends CategoryStoreRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => [
                'required',
                'string',
                Rule::unique('categories')->ignore($this->route()->parameter('category')),
            ],
            'description' => 'required|string',
            'score_from' => 'required|integer|min:0|max:1000',
            'score_to' => 'required|integer|min:0|max:1000',
        ];
    }
}
