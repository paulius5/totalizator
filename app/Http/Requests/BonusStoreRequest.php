<?php

declare (strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class BonusStoreRequest
 * @package App\Http\Requests
 */
class BonusStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'amount' => 'required|integer',
            'description' => 'nullable|string|max:191',
        ];
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return (int)$this->input('amount');
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->input('description');
    }

    /**
     * @return bool
     */
    public function getActive(): bool
    {
        return (bool)$this->input('active');
    }
}
