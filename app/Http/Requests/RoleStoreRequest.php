<?php

declare (strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class RoleStoreRequest
 * @package App\Http\Requests
 */
class RoleStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|unique:roles|alpha',
            'description' => 'required|string',
        ];
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->input('name');
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->input('description');
    }

    /**
     * @return string|null
     */
    public function getDefault(): ?string
    {
        return $this->input('default');
    }

    /**
     * @return string|null
     */
    public function getFullAccess(): ?string
    {
        return $this->input('full_access');
    }

    /**
     * @return string|null
     */
    public function getActive(): ?string
    {
        return $this->input('active');
    }

    /**
     * @return array|null
     */
    public function getPermissionsIds(): ?array
    {
        return $this->input('permissions');
    }
}
