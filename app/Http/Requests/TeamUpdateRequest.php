<?php

declare (strict_types=1);

namespace App\Http\Requests;

use Illuminate\Validation\Rule;

/**
 * Class TeamUpdateRequest
 * @package App\Http\Requests
 */
class TeamUpdateRequest extends TeamStoreRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => [
                'required',
                'string',
                Rule::unique('teams')->ignore($this->route()->parameter('team')),
            ],
            'description' => 'required|string',
            'category_id' => 'required|integer|exists:categories,id',
            'subcategory_id' => 'required|integer|exists:subcategories,id',
        ];
    }
}
