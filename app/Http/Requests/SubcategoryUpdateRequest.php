<?php

declare (strict_types=1);

namespace App\Http\Requests;

use Illuminate\Validation\Rule;

class SubcategoryUpdateRequest extends SubcategoryStoreRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => [
                'required',
                'string',
                'max:50',
                Rule::unique('subcategories')->ignore($this->route()->parameter('subcategory')),
            ],
            'description' => 'required|string',
            'category_id' => 'required|integer|exists:categories,id',
        ];
    }
}
