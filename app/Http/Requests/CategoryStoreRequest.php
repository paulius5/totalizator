<?php

declare (strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CategoryStoreRequest
 * @package App\Http\Requests
 */
class CategoryStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|unique:categories',
            'description' => 'required|string',
            'score_from' => 'required|integer|min:0|max:1000',
            'score_to' => 'required|integer|min:0|max:1000',
        ];
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->input('name');
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->input('description');
    }

    /**
     * @return int
     */
    public function getScoreFrom(): int
    {
        return (int)$this->input('score_from');
    }

    /**
     * @return int
     */
    public function getScoreTo(): int
    {
        return (int)$this->input('score_to');
    }
}
