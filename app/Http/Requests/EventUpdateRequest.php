<?php

declare (strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class EventUpdateRequest
 * @package App\Http\Requests
 */
class EventUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'open_bet_at' => 'required',
            'close_bet_at' => 'required',
            'start_game_at' => 'required',
            'finish_game_at' => 'required',
        ];
    }

    /**
     * @return string
     */
    public function getOpenBetAt(): string
    {
        return $this->input('open_bet_at');
    }

    /**
     * @return string
     */
    public function getCloseBetAt(): string
    {
        return $this->input('close_bet_at');
    }

    /**
     * @return string
     */
    public function getStartGameAt(): string
    {
        return $this->input('start_game_at');
    }

    /**
     * @return string
     */
    public function getFinishGameAt(): string
    {
        return $this->input('finish_game_at');
    }
}
