<?php

declare (strict_types=1);

namespace App\Http\Middleware;

use Auth;
use Closure;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;

/**
 * Class CheckPermission
 * @package App\Http\Middleware
 */
class CheckPermission
{
    /**
     * @param $request
     * @param Closure $next
     * @param $permission
     * @return RedirectResponse|Redirector|mixed
     */
    public function handle($request, Closure $next, $permission)
    {
        $roles = Auth::user()->roles()->where('active', 1)->with('permissions')->get();

        if (!$request->user()->canAccess($permission, $roles)) {
            return redirect('forbidden');
        }
        return $next($request);
    }
}
