<?php

declare (strict_types=1);

namespace App\Http\Controllers;

use App\Repositories\BetRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

/**
 * Class BetController
 * @package App\Http\Controllers
 */
class BetController extends Controller
{
    /**
     *
     */
    const PER_PAGE = 10;

    /**
     * @var BetRepository
     */
    private $betRepository;

    /**
     * BetController constructor.
     * @param BetRepository $betRepository
     */
    public function __construct(BetRepository $betRepository)
    {
        $this->betRepository = $betRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     * @throws \Exception
     */
    public function index(): View
    {
        $bets = $this->betRepository
            ->with(['event.team1', 'event.team2', 'user', 'event.results'])
            ->paginate(self::PER_PAGE);

        return view('bet.list', compact('bets'));
    }

    /**
     * @return View
     * @throws \Exception
     */
    public function companyOpenBets(): View
    {
        $bets = $this->betRepository
            ->with(['event.team1', 'event.team2'])
            ->where('closed', 0)
            ->paginate(self::PER_PAGE);

        return view('bet.list', compact('bets'));
    }

    /**
     * @return View
     * @throws \Exception
     */
    public function companyClosedBets(): View
    {
        $bets = $this->betRepository
            ->with(['event.team1', 'event.team2'])
            ->where('closed', 1)
            ->paginate(self::PER_PAGE);

        return view('bet.list', compact('bets'));
    }

    /**
     * @return View
     * @throws \Exception
     */
    public function playerOpenBets(): View
    {
        $bets = $this->betRepository
            ->with(['event'])
            ->where('user_id', Auth::id())
            ->where('closed', 0)
            ->paginate(self::PER_PAGE);

        return view('bet.list', compact('bets'));
    }

    /**
     * @return View
     * @throws \Exception
     */
    public function playerClosedBets(): View
    {
        $bets = $this->betRepository
            ->with(['event.team1', 'event.team2', 'event.results'])
            ->where('user_id', Auth::id())
            ->where('closed', 1)
            ->paginate(self::PER_PAGE);

        return view('bet.list', compact('bets'));
    }
}