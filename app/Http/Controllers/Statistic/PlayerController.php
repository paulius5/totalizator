<?php

declare(strict_types = 1);

namespace App\Http\Controllers\Statistic;

use App\Console\Commands\Statistic\CachePlayerStatistic;
use App\Http\Controllers\Controller;
use App\Services\Statistic\PlayerService;
use Auth;
use Cache;
use Illuminate\View\View;

/**
 * Class PlayerController
 * @package App\Http\Controllers\Statistic
 */
class PlayerController extends Controller
{
    /**
     * @var PlayerService
     */
    private $playerService;

    /**
     * PlayerController constructor.
     * @param PlayerService $playerService
     */
    public function __construct(PlayerService $playerService)
    {
        $this->playerService = $playerService;
    }

    /**
     * @return View
     */
    public function index(): View
    {
        $stats = [];

        foreach (CachePlayerStatistic::BETS_FILTER as $filter) {
            foreach (CachePlayerStatistic::TIME_INDEXES as $index) {
                $info = Cache::tags("user_" . Auth::id())->get($index[0] . $index[1] . "_" . $filter['key_name']);
                if ($info) {
                    $stats[$filter['key_name']][] = array_add($info, 'description', $index[2]);
                }
            }
        }

        return view('statistic.player', compact('stats'));
    }
}
