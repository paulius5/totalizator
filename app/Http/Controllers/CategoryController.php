<?php

declare (strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\CategoryStoreRequest;
use App\Http\Requests\CategoryUpdateRequest;
use App\Repositories\CategoryRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class CategoryController extends Controller
{
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @return View
     * @throws \Exception
     */
    public function index(): View
    {
        $categories = $this->categoryRepository->paginate();

        return view('category.list', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        return view('category.create');
    }

    /**
     * @param CategoryStoreRequest $request
     * @return RedirectResponse
     * @throws \Exception
     */
    public function store(CategoryStoreRequest $request)
    {
        $this->categoryRepository->create([
            'name' => $request->getName(),
            'description' => $request->getDescription(),
            'score_from' => $request->getScoreFrom(),
            'score_to' => $request->getScoreTo(),
        ]);

        return redirect()
            ->route('category.index')
            ->with('status', 'Category created successfully!');
    }

    /**
     * @param int $categoryId
     * @return View
     * @throws \Exception
     */
    public function show(int $categoryId): View
    {
        $category = $this->categoryRepository->find($categoryId);

        return view('category.view', compact('category'));
    }

    /**
     * @param int $categoryId
     * @return \Illuminate\Contracts\View\Factory|View
     * @throws \Exception
     */
    public function edit(int $categoryId)
    {
        $category = $this->categoryRepository->find($categoryId);

        return view('category.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CategoryUpdateRequest $request
     * @param int $categoryId
     * @return RedirectResponse
     * @throws \Exception
     */
    public function update(CategoryUpdateRequest $request, int $categoryId): RedirectResponse
    {
        $data = [
            'name' => $request->getName(),
        'description' => $request->getDescription(),
        'score_from' => $request->getScoreFrom(),
        'score_to' => $request->getScoreTo(),
        ];

        $this->categoryRepository->update($data, $categoryId);

        return redirect()
            ->route('category.index')
            ->with('status', 'Category updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $categoryId
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $categoryId): RedirectResponse
    {
        $this->categoryRepository->delete(['id' => $categoryId]);

        return redirect()
            ->route('category.index')
            ->with('status', 'Category deleted successfully!');
    }
}
