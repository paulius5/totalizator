<?php

declare (strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\SubcategoryStoreRequest;
use App\Http\Requests\SubcategoryUpdateRequest;
use App\Repositories\CategoryRepository;
use App\Repositories\SubcategoryRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class SubcategoryController extends Controller
{
    /**
     * @var SubcategoryRepository
     */
    private $subcategoryRepository;
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository,SubcategoryRepository $subcategoryRepository)
    {
        $this->subcategoryRepository = $subcategoryRepository;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     * @throws \Exception
     */
    public function index(): View
    {
        $subcategories = $this->subcategoryRepository->paginate();

        return view('subcategory.list', compact('subcategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     * @throws \Exception
     */
    public function create(): View
    {
        $categories = $this->categoryRepository->all();

        return view('subcategory.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SubcategoryStoreRequest $request
     * @return RedirectResponse
     * @throws \Exception
     */
    public function store(SubcategoryStoreRequest $request): RedirectResponse
    {
        $this->subcategoryRepository->create([
            'name' => $request->getName(),
            'description' => $request->getDescription(),
            'category_id' => $request->getCategoryId(),
        ]);

        return redirect()
            ->route('subcategory.index')
            ->with('status', 'Subcategory created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param int $subcategoryId
     * @return View
     * @throws \Exception
     */
    public function show(int $subcategoryId): View
    {
        $subcategory = $this->subcategoryRepository->find($subcategoryId);

        return view('subcategory.view', compact('subcategory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $subcategoryId
     * @return View
     * @throws \Exception
     */
    public function edit(int $subcategoryId): View
    {
        $categories = $this->categoryRepository->all();

        $subcategory = $this->subcategoryRepository->find($subcategoryId);

        return view('subcategory.edit', compact('subcategory', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param SubcategoryUpdateRequest $request
     * @param int $subcategoryId
     * @return RedirectResponse
     * @throws \Exception
     */
    public function update(SubcategoryUpdateRequest $request, int $subcategoryId): RedirectResponse
    {
        $data = [
        'name' => $request->getName(),
        'description' => $request->getDescription(),
        'category_id' => $request->getCategoryId(),
        ];

        $this->subcategoryRepository->update($data, $subcategoryId);

        return redirect()
            ->route('subcategory.index')
            ->with('status', 'Subcategory updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $subcategoryId
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $subcategoryId): RedirectResponse
    {
        $this->subcategoryRepository->delete(['id' => $subcategoryId]);

        return redirect()
            ->route('subcategory.index')
            ->with('status', 'Subcategory deleted successfully');
    }
}