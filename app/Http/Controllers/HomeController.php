<?php

declare (strict_types=1);

namespace App\Http\Controllers;

use Illuminate\View\View;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return void
     */
    public function index()
    {
        //
    }

    /**
     * @return View
     */
    public function error(): View
    {
        return view('errors.404');
    }

    /**
     * @return View
     */
    public function forbidden(): View
    {
        return view('errors.401');
    }
}
