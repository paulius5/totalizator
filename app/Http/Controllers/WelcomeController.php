<?php

declare (strict_types=1);

namespace App\Http\Controllers;

use App\Repositories\BonusRepository;
use App\Repositories\EventRepository;
use Illuminate\View\View;

/**
 * Class WelcomeController
 * @package App\Http\Controllers
 */
class WelcomeController extends Controller
{
    /** @var BonusRepository */
    private $bonusRepository;

    private $eventRepository;

    /**
     * WelcomeController constructor.
     * @param BonusRepository $bonusRepository
     * @param EventRepository $eventRepository
     */
    public function __construct(BonusRepository $bonusRepository, EventRepository $eventRepository)
    {
        $this->bonusRepository = $bonusRepository;
        $this->eventRepository = $eventRepository;
    }

    /**
     * @return View
     * @throws \Exception
     */
    public function welcome(): View
    {
        $bonus = $this->bonusRepository
            ->where('active', 1)
            ->first();

        $events = $this->eventRepository->random()->with(['team1', 'team2'])->limit(5)->get();

        return view('welcome', compact('bonus', 'events'));
    }
}
