<?php

declare (strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\RoleStoreRequest;
use App\Http\Requests\RoleUpdateRequest;
use App\Permission;
use App\Repositories\RoleRepository;
use App\Role;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Throwable;

/**
 * Class RoleController
 * @package App\Http\Controllers
 */
class RoleController extends Controller
{
    /**
     * @var RoleRepository
     */
    private $roleRepository;

    /**
     * RoleController constructor.
     * @param RoleRepository $roleRepository
     */
    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     * @throws \Exception
     */
    public function index(): View
    {
        $roles = $this->roleRepository->sortBy('name')->paginate();

        return view('role.list', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        $permissions = Permission::all()->sortBy('name');

        return view('role.create', compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param RoleStoreRequest $request
     * @return RedirectResponse
     * @throws \Exception
     */
    public function store(RoleStoreRequest $request): RedirectResponse
    {
        /** @var Role $role */
        $role = $this->roleRepository->create([
            'name' => $request->getName(),
            'description' => $request->getDescription(),
            'default' => boolval($request->getDefault()),
            'full_access' => boolval($request->getFullAccess()),
            'active' => boolval($request->getActive()),
        ]);

        if (!boolval($request->getFullAccess())) {
            $role->permissions()->sync($request->getPermissionsIds());
        } else {
            $role->permissions()->sync([]);
        }

        return redirect()
            ->route('role.index')
            ->with('status', 'Role created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param int $roleId
     * @return View
     * @throws \Exception
     */
    public function show(int $roleId): View
    {
        $role = $this->roleRepository->find($roleId);

        return view('role.view', compact('role'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $roleId
     * @return View
     * @throws \Exception
     */
    public function edit(int $roleId): View
    {
        $role = $this->roleRepository->with(['permissions'])->find($roleId);
        $permissions = Permission::all()->sortBy('name');

        return view('role.edit', compact('role', 'permissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param RoleUpdateRequest $request
     * @param int $roleId
     * @return RedirectResponse
     * @throws \Exception
     */
    public function update(RoleUpdateRequest $request, int $roleId): RedirectResponse
    {
        $data = [
            'name' => $request->getName(),
            'description' => $request->getDescription(),
            'default' => boolval($request->getDefault()),
            'full_access' => boolval($request->getFullAccess()),
            'active' => boolval($request->getActive()),
        ];

        /** @var Role $role */
        $this->roleRepository->update($data, $roleId);

        $role = $this->roleRepository->find($roleId);

        if (!boolval($request->getFullAccess())) {
            $role->permissions()->sync($request->getPermissionsIds());
        } else {
            $role->permissions()->sync([]);
        }

        return redirect()
            ->route('role.index')
            ->with('status', 'Role updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $roleId
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $roleId): RedirectResponse
    {
        try {
            if ($this->roleRepository->delete(['id' => $roleId])) {
                return redirect()
                    ->route('role.index')
                    ->with('status', 'Role deleted successfully!');
            } else {
                return redirect()
                    ->route('role.index')
                    ->with('error', 'Role with ID: ' . $roleId . ' does not exists!');
            }
        } catch (Throwable $exception) {
            return redirect()
                ->route('role.index')
                ->with('error', $exception->getMessage());
        }
    }
}
