<?php

declare (strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\UserUpdateRequest;
use App\Repositories\RoleRepository;
use App\Repositories\UserRepository;
use App\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

/**
 * Class UserController
 * @package App\Http\Controllers
 */
class UserController extends Controller
{
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var RoleRepository
     */
    private $roleRepository;

    /**
     * UserController constructor.
     * @param UserRepository $userRepository
     * @param RoleRepository $roleRepository
     */
    public function __construct(UserRepository $userRepository, RoleRepository $roleRepository)
    {
        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     * @throws \Exception
     */
    public function index(): View
    {
        $users = $this->userRepository->paginate();

        return view('user.list', compact('users'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $userId
     * @return View
     * @throws \Exception
     */
    public function show(int $userId): View
    {
        $user = $this->userRepository->find($userId);

        return view('user.view', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $userId
     * @return View
     * @throws \Exception
     */
    public function edit(int $userId): View
    {
        $user = $this->userRepository->find($userId);

        $roles = $this->roleRepository->all();

        return view('user.edit', compact('user', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UserUpdateRequest $request
     * @param int $userId
     * @return RedirectResponse
     * @throws \Exception
     */
    public function update(UserUpdateRequest $request, int $userId): RedirectResponse
    {
        $data = [
            'name' => $request->getName(),
            'email' => $request->getEmail(),
        ];

        $this->userRepository->update($data, $userId);

        $user = $this->userRepository->find($userId);

        /** @var User $user */
        $user->roles()->sync($request->getRoles());

        return redirect()
            ->route('user.index')
            ->with('status', 'User updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $userId
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $userId): RedirectResponse
    {
        if ($this->userRepository->delete(['id' => $userId])) {

            return redirect()
                ->route('user.index')
                ->with('status', 'User deleted successfully!');
        } else {
            return redirect()
                ->route('user.index')
                ->with('error', 'No such user with ID: ' . $userId . '!');
        }
    }

    /**
     * @return View
     * @throws \Exception
     */
    public function info(): View
    {
        $user = $this->userRepository->with(['roles'])->find(Auth::id());

        return view('user.info', compact('user'));
    }
}
