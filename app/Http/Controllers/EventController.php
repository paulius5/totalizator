<?php

declare (strict_types=1);

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\EventUpdateRequest;
use App\Repositories\EventRepository;
use App\Services\EventService;
use App\Subcategory;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

/**
 * Class EventController
 * @package App\Http\Controllers
 */
class EventController extends Controller
{
    /**
     * @var EventService
     */
    private $eventService;
    /**
     * @var EventRepository
     */
    private $eventRepository;

    /**
     * EventController constructor.
     * @param EventService $eventService
     * @param EventRepository $eventRepository
     */
    public function __construct(EventService $eventService, EventRepository $eventRepository)
    {
        $this->eventService = $eventService;
        $this->eventRepository = $eventRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     * @throws \Exception
     */
    public function index(): View
    {
        $events = $this->eventRepository
            ->with(['team1', 'team2'])
            ->orderBy('id', 'desc')
            ->paginate();

        return view('event.list', compact('events'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $eventId
     * @return View
     * @throws \Exception
     */
    public function show(int $eventId): View
    {
        $event = $this->eventRepository->with(['team1', 'team2', 'team1.subcategory.category'])->find($eventId);

        return view('event.view', compact('event'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $eventId
     * @return View
     * @throws \Exception
     */
    public function edit(int $eventId): View
    {
        $event = $this->eventRepository->with(['team1', 'team2', 'team1.subcategory.category'])->find($eventId);

        return view('event.edit', compact('event'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param EventUpdateRequest $request
     * @param int $eventId
     * @return RedirectResponse
     * @throws \Exception
     */
    public function update(EventUpdateRequest $request, int $eventId): RedirectResponse
    {
        $data = [
            'open_bet_at' => $request->getOpenBetAt(),
            'close_bet_at' => $request->getCloseBetAt(),
            'start_game_at' => $request->getStartGameAt(),
            'finish_game_at' => $request->getFinishGameAt(),
        ];

        $this->eventRepository->update($data, $eventId);

        return redirect()
            ->route('event.index')
            ->with('status', 'Event updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $eventId
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $eventId): RedirectResponse
    {
        $this->eventRepository->delete(['id' => $eventId]);

        return redirect()
            ->route('event.index')
            ->with('status', 'Event deleted successfully!');
    }

    /**
     * @return View
     */
    public function showAvailableEvents(): View
    {
        $events = $this->eventRepository->getAvailableEvents();

        $categories = Category::all()->toArray();
        $subcategories = Subcategory::all()->toArray();

        return view('event.available', compact('events', 'categories', 'subcategories'));
    }
}
