<?php

declare(strict_types = 1);

namespace App\Http\Controllers\API;

use App\Repositories\BalanceHistoryRepository;
use App\Repositories\BetRepository;
use App\Repositories\UserRepository;
use App\Services\EventService;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

/**
 * Class BetController
 * @package App\Http\Controllers\API
 */
class BetController extends Controller
{
    /**
     * @var EventService $eventService
     */
    private $eventService;

    /**
     * @var BetRepository $betRepository
     */
    private $betRepository;

    /**
     * @var UserRepository $userRepository
     */
    private $userRepository;

    /**
     * @var BalanceHistoryRepository $balanceHistoryRepository
     */
    private $balanceHistoryRepository;

    /**
     * BetController constructor.
     * @param EventService $eventService
     * @param BetRepository $betRepository
     * @param UserRepository $userRepository
     * @param BalanceHistoryRepository $balanceHistoryRepository
     */
    public function __construct(
        EventService $eventService,
        BetRepository $betRepository,
        UserRepository $userRepository,
        BalanceHistoryRepository $balanceHistoryRepository
    )
    {
        $this->eventService = $eventService;
        $this->betRepository = $betRepository;
        $this->userRepository = $userRepository;
        $this->balanceHistoryRepository = $balanceHistoryRepository;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     * @throws Exception
     */
    public function place(Request $request)
    {
        try {
            $validator = $request->validate([
                'bet_for' => [
                    'required',
                    Rule::in([0, 1, 2]),
                ],
                'bet_amount' => 'required|min:1|max:555',
            ]);
        } catch (Exception $e) {
            return response()->json($e);
        }

        if (!$validator) {
            return response()->json($validator);
        }

        if (!$this->enoughMoney($request->user_id, $request->bet_amount)) {
            return response()->json(['errors' => 'You don\'t have enough money to place bet.'], Response::HTTP_NOT_ACCEPTABLE);
        }

        if (!$this->eventService->eventClosed($request->event_id)) {
            $this->betRepository->create([

                'user_id' => $request->user_id,
                'event_id' => $request->event_id,
                'bet_for' => $request->bet_for,
                'bet_rate' => $this->eventService->getRate($request->event_id, (int)$request->bet_for),
                'bet_amount' => $request->bet_amount,
            ]);

            $user = $this->userRepository->find($request->user_id);

            $this->balanceHistoryRepository
                ->create([
                    'user_id' => $user->id,
                    'amount' => $request->bet_amount,
                    'balance' => $user->balance,
                ]);

            $this->userRepository
                ->where('id', Auth::id())
                ->decrement('balance', $request->bet_amount);

            return response()->json('Your bet placed successfully!');
        } else {
            return response()->json(
                ['errors' => 'It is not allowed to place bet for this event, because it is already closed.'],
                Response::HTTP_NOT_ACCEPTABLE
            );
        }
    }

    /**
     * @param $user_id
     * @return float
     * @throws Exception
     */
    private function userBalance($user_id): float
    {
        $userRepository = app(UserRepository::class);

        return $userRepository
            ->where('id', $user_id)
            ->get()
            ->pluck('balance')[0];
    }

    /**
     * @param $user_id
     * @param $money
     * @return bool
     * @throws Exception
     */
    private function enoughMoney($user_id, $money): bool
    {
        return $this->userBalance($user_id) > $money;
    }
}
