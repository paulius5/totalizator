<?php

declare(strict_types = 1);

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Services\API\EventService;
use Illuminate\Support\Collection;

/**
 * Class EventController
 * @package App\Http\Controllers\API
 */
class EventController extends Controller
{
    /**
     * @var EventService
     */
    private $eventService;

    /**
     * EventController constructor.
     * @param $eventService
     */
    public function __construct(EventService $eventService)
    {
        $this->eventService = $eventService;
    }

    /**
     * @return Collection
     */
    public function index(): Collection
    {
        return $this->eventService->availableEvents();
    }
}
