<?php

declare(strict_types = 1);

namespace App\Http\Controllers\API;

use App\Services\API\CategoryService;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class CategoryController
 * @package App\Http\Controllers\API
 */
class CategoryController extends Controller
{
    /**
     * @var CategoryService
     */
    private $categoryService;

    /**
     * CategoryController constructor.
     * @param CategoryService $categoryService
     */
    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    /**
     * @return Builder[]|Collection
     * @throws \Exception
     */
    public function index()
    {
        return $this->categoryService->categoriesWithSubcategories();
    }
}
