<?php

declare (strict_types=1);

namespace App\Http\Controllers\Auth;

use App\BalanceHistory;
use App\Bonus;
use App\Role;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

/**
 * Class RegisterController
 * @package App\Http\Controllers\Auth
 */
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $bonus = Bonus::where('active', 1)->first();

        $data = [
            'name'      => $data['name'],
            'email'     => $data['email'],
            'password'  => Hash::make($data['password']),
        ];

        if ($bonus) {
            array_set($data, 'bonus_id', $bonus->id);
            array_set($data, 'balance', $bonus->amount);
        }

        $user = User::create($data);

        $role = Role::where('default', 1)->get();
        if ($role) {
            $user->roles()->sync($role);
        }

        if ($bonus) {
            BalanceHistory::create([
                'user_id' => $user->id,
                'amount' => $bonus->amount,
                'balance' => 0,
                'sign' => '+',
                'message' => 'Registration bonus',
            ]);
        }

        return $user;
    }
}
