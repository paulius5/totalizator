<?php

declare (strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\TeamStoreRequest;
use App\Http\Requests\TeamUpdateRequest;
use App\Repositories\CategoryRepository;
use App\Repositories\SubcategoryRepository;
use App\Repositories\TeamRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

/**
 * Class TeamController
 * @package App\Http\Controllers
 */
class TeamController extends Controller
{
    /**
     * @var TeamRepository
     */
    private $teamRepository;
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;
    /**
     * @var SubcategoryRepository
     */
    private $subcategoryRepository;

    public function __construct(TeamRepository $teamRepository, CategoryRepository $categoryRepository, SubcategoryRepository $subcategoryRepository)
    {
        $this->teamRepository = $teamRepository;
        $this->categoryRepository = $categoryRepository;
        $this->subcategoryRepository = $subcategoryRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     * @throws \Exception
     */
    public function index(): View
    {
        $teams = $this->teamRepository->with(['subcategory.category'])->paginate();

        return view('team.list', compact('teams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     * @throws \Exception
     */
    public function create(): View
    {
        $categories = $this->categoryRepository->all();
        $subcategories = $this->subcategoryRepository->all();

        return view('team.create', compact('categories', 'subcategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param TeamStoreRequest $request
     * @return RedirectResponse
     * @throws \Exception
     */
    public function store(TeamStoreRequest $request): RedirectResponse
    {
        $this->teamRepository->create([
            'name' => $request->getName(),
            'description' => $request->getDescription(),
            'subcategory_id' => $request->getSubcategoryId(),
        ]);

        return redirect()
            ->route('team.index')
            ->with('status', 'Team created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param int $teamId
     * @return View
     * @throws \Exception
     */
    public function show(int $teamId): View
    {
        $team = $this->teamRepository->find($teamId);

        return view('team.view', compact('team'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $teamId
     * @return View
     * @throws \Exception
     */
    public function edit(int $teamId): View
    {
        $categories = $this->categoryRepository->all();
        $subcategories = $this->subcategoryRepository->all();
        $team = $this->teamRepository->find($teamId);

        return view('team.edit', compact('team', 'categories', 'subcategories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param TeamUpdateRequest $request
     * @param int $teamId
     * @return RedirectResponse
     * @throws \Exception
     */
    public function update(TeamUpdateRequest $request, int $teamId): RedirectResponse
    {
        $data = [
            'name' => $request->getName(),
            'description' => $request->getDescription(),
            'subcategory_id' => $request->getSubcategoryId(),
        ];

        $this->teamRepository->update($data, $teamId);

        return redirect()
            ->route('team.index')
            ->with('status', 'Team updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $teamId
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $teamId): RedirectResponse
    {
        $this->teamRepository->delete([$teamId]);

        return redirect()
            ->route('team.index')
            ->with('status', 'Team deleted successfully!');
    }
}
