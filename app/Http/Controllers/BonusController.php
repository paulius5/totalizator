<?php

declare (strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\BonusStoreRequest;
use App\Repositories\BonusRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

/**
 * Class BonusController
 * @package App\Http\Controllers
 */
class BonusController extends Controller
{
    /** @var BonusRepository */
    private $bonusRepository;

    /**
     * BonusController constructor.
     * @param BonusRepository $bonusRepository
     */
    public function __construct(BonusRepository $bonusRepository)
    {
        $this->bonusRepository = $bonusRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     * @throws \Exception
     */
    public function index(): View
    {
        $bonuses = $this->bonusRepository
            ->makeQuery()
            ->orderBy('active', 'desc')
            ->orderBy('created_at', 'desc')
            ->paginate();

        return view('bonus.list', compact('bonuses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        return view('bonus.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param BonusStoreRequest $request
     * @return RedirectResponse
     * @throws \Exception
     */
    public function store(BonusStoreRequest $request): RedirectResponse
    {
        if ($request->getActive()) {
            $this->bonusRepository
                ->where('id', 'like', '%')
                ->update(['active' => 0]);
        }

        $this->bonusRepository
            ->create([
                'amount' => $request->getAmount(),
                'description' => $request->getDescription(),
                'active' => $request->getActive(),
            ]);

        return redirect()
            ->route('bonus.index')
            ->with('status', 'Bonus successfully created!');
    }

    /**
     * @param int $bonusId
     * @return RedirectResponse
     * @throws \Exception
     */
    public function activate(int $bonusId): RedirectResponse
    {
        $bonus = $this->bonusRepository->find($bonusId);

        if ($bonus) {
            $this->bonusRepository
                ->where('active', 1)
                ->update(['active' => 0]);

            $this->bonusRepository
                ->update([
                    'active' => 1
                ], $bonusId);

            return redirect()
                ->route('bonus.index')
                ->with('status', 'Bonus with ID: ' . $bonusId . ' successfully activated!');
        } else {
            return redirect()
                ->route('bonus.index')
                ->with('error', 'There is no bonus with ID: ' . $bonusId);
        }
    }

    /**
     * @return RedirectResponse
     * @throws \Exception
     */
    public function deactivate(): RedirectResponse
    {
            $this->bonusRepository
                ->where('active', 1)
                ->update(['active' => 0]);

            return redirect()
                ->route('bonus.index')
                ->with('status', 'All bonuses deactivated!');

    }
}
