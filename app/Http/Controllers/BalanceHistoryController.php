<?php

declare (strict_types=1);

namespace App\Http\Controllers;

use App\Repositories\BalanceHistoryRepository;
use App\Repositories\UserRepository;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

/**
 * Class BalanceHistoryController
 * @package App\Http\Controllers
 */
class BalanceHistoryController extends Controller
{
    /** @var BalanceHistoryRepository */
    private $balanceHistoryRepository;

    /** @var UserRepository */
    private $userRepository;

    /**
     * BalanceHistoryController constructor.
     * @param BalanceHistoryRepository $balanceHistoryRepository
     * @param UserRepository $userRepository
     */
    public function __construct(BalanceHistoryRepository $balanceHistoryRepository, UserRepository $userRepository)
    {
        $this->balanceHistoryRepository = $balanceHistoryRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @throws \Exception
     */
    public function show(): View
    {
        /** @var User $user */
        $user = Auth::user();

        $balanceHistory = $user->balanceHistoryRecords()
            ->orderBy('id', 'desc')
            ->paginate();

        return view('balance.list', compact('user', 'balanceHistory'));
    }
}
