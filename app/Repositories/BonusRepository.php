<?php

declare (strict_types=1);

namespace App\Repositories;

use App\Bonus;

/**
 * Class BonusRepository
 * @package App\Repositories
 */
class BonusRepository extends Repository
{
    /**
     * @return string
     */
    public function model(): string
    {
        return Bonus::class;
    }
}