<?php

declare (strict_types=1);

namespace App\Repositories;

use App\Team;

/**
 * Class TeamRepository
 * @package App\Repositories
 */
class TeamRepository extends Repository
{
    /**
     * @return string
     */
    public function model(): string
    {
        return Team::class;
    }
}