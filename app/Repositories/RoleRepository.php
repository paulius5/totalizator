<?php

declare (strict_types=1);

namespace App\Repositories;

use App\Role;

class RoleRepository extends Repository
{
    public function model(): string
    {
        return Role::class;
    }
}