<?php

declare (strict_types=1);

namespace App\Repositories;

use App\BalanceHistory;

/**
 * Class BalanceHistoryRepository
 * @package App\Repositories
 */
class BalanceHistoryRepository extends Repository
{
    /**
     * @return string
     */
    public function model(): string
    {
        return BalanceHistory::class;
    }
}