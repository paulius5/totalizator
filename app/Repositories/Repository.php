<?php

declare (strict_types=1);

namespace App\Repositories;

use App\Contracts\RepositoryContract;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

/**
 * Class Repository
 * @package App\Repositories
 */
abstract class Repository implements RepositoryContract
{
    /**
     *
     */
    const DEFAULT_FIELD_NAME = 'id';

    /**
     * @return string
     */
    abstract public function model(): string;

    /**
     * @param array $columns
     * @return Collection
     * @throws Exception
     */
    public function all(array $columns = ['*']): Collection
    {
        return $this->makeQuery()->get($columns);
    }

    /**
     * @param array $data
     * @return Builder|Model
     * @throws Exception
     */
    public function create(array $data = [])
    {
        return $this->makeQuery()->create($data);
    }

    /**
     * @param int $id
     * @return Model
     * @throws Exception
     */
    public function find(int $id): ?Model
    {
        return $this->makeQuery()->find($id);
    }

    /**
     * @param array $data
     * @param $fieldValue
     * @param string $fieldName
     * @return int
     * @throws Exception
     */
    public function update(array $data, $fieldValue, string $fieldName = self::DEFAULT_FIELD_NAME): int
    {
        return $this->makeQuery()->where($fieldName, $fieldValue)->update($data);
    }

    /**
     * @param $column
     * @param int $amount
     * @param array $extra
     * @throws Exception
     */
    public function increment($column, $amount = 1, array $extra = [])
    {
        $this->makeQuery()->increment($column, $amount, $extra);
    }

    /**
     * @param $column
     * @param int $amount
     * @param array $extra
     * @throws Exception
     */
    public function decrement($column, $amount = 1, array $extra = [])
    {
        $this->makeQuery()->decrement($column, $amount, $extra);
    }

    /**
     * @param array $criteria
     * @return mixed
     * @throws Exception
     */
    public function delete(array $criteria = [])
    {
        return $this->makeQuery()->where($criteria)->delete();
    }

    /**
     * @param array $relations
     * @return Builder
     * @throws Exception
     */
    public function with(array $relations = []): Builder
    {
        return $this->makeQuery()->with($relations);
    }

    /**
     * @param $column
     * @param null $operator
     * @param null $value
     * @param string $boolean
     * @return Builder
     * @throws Exception
     */
    public function where($column, $operator = null, $value = null, $boolean = 'and')
    {
        return $this->makeQuery()->where($column, $operator, $value, $boolean);
    }

    /**
     * @param null $perPage
     * @param array $columns
     * @param string $pageName
     * @param null $page
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     * @throws Exception
     */
    public function paginate($perPage = null, $columns = ['*'], $pageName = 'page', $page = null)
    {
        return $this->makeQuery()->paginate($perPage, $columns, $pageName, $page);
    }

    /**
     * @param $column
     * @param string $direction
     * @return mixed
     * @throws Exception
     */
    public function sortBy($column, $direction = 'asc')
    {
        return $this->makeQuery()->orderBy($column, $direction);
    }

    /**
     * @param string $seed
     * @return Builder
     * @throws Exception
     */
    public function random($seed = '')
    {
        return $this->makeQuery()->inRandomOrder($seed);
    }

    /**
     * @return Model
     * @throws Exception
     */
    final protected function makeModel(): Model
    {
        $model = app($this->model());

        if (!$model instanceof Model) {
            throw new Exception('Class ' . $this->model() . ' must be instance of Illuminate\\Database\\Eloquent\\Model');
        }

        return $model;
    }

    /**
     * @return Builder
     * @throws Exception
     */
    public function makeQuery(): Builder
    {
        return $this->makeModel()->newQuery();
    }
}