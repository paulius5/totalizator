<?php

declare (strict_types=1);

namespace App\Repositories;

use App\Bet;
use Exception;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Collection;

/**
 * Class BetRepository
 * @package App\Repositories
 */
class BetRepository extends Repository
{
    /**
     * @return string
     */
    public function model(): string
    {
        return Bet::class;
    }

    /**
     * @param $eventId
     * @return Collection
     * @throws Exception
     */
    public function getBetsInformationWhichNotClosedByEventId($eventId): Collection
    {
        return collect($this->makeQuery()
            ->join('results', function (JoinClause $q) use ($eventId) {
                $q->on('bets.event_id', 'results.event_id')
                    ->where('bets.event_id', '=', $eventId)
                    ->where('bets.closed', '=', 0);
            })
            ->select(['bets.*', 'results.team1_score', 'team2_score'])
            ->get()->toArray());
    }
}