<?php

declare (strict_types=1);

namespace App\Repositories;

use App\Event;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 * Class EventRepository
 * @package App\Repositories
 */
class EventRepository extends Repository
{
    /**
     * @return string
     */
    public function model(): string
    {
        return Event::class;
    }

    /**
     * @return Collection
     */
    public function getSubcategoriesIdsWhichHave2OrMoreTeams(): Collection
    {
        return DB::table('teams')
            ->select('id')
            ->select(DB::raw('count(*) as count, subcategory_id as id'))
            ->having('count', '>', 1)
            ->groupBy('subcategory_id')
            ->get()->pluck('id');
    }

    /**
     * @return Collection
     */
    public function getAvailableEvents(): Collection
    {
        $now = Carbon::now();
        return Event::with(['team1', 'team2', 'team1.subcategory.category'])
            ->where('close_bet_at', '>', $now)
            ->where('open_bet_at', '<=', $now)
            ->get();
    }
}