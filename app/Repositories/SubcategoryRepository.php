<?php

declare (strict_types=1);

namespace App\Repositories;

use App\Subcategory;

/**
 * Class SubcategoryRepository
 * @package App\Repositories
 */
class SubcategoryRepository extends Repository
{
    /**
     * @return string
     */
    public function model(): string
    {
        return Subcategory::class;
    }
}