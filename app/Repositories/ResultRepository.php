<?php

declare (strict_types=1);

namespace App\Repositories;

use App\Result;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 * Class ResultRepository
 * @package App\Repositories
 */
class ResultRepository extends Repository
{
    /**
     * @return string
     */
    public function model(): string
    {
        return Result::class;
    }

    /**
     * @param $ids
     * @return Collection
     */
    public function getCategoriesIdsForFinishedGamesExceptWithResults($ids): Collection
    {
        return DB::table('events')
            ->select(['events.id as id', 'categories.score_from', 'categories.score_to'])
            ->join('teams', 'events.team1_id', 'teams.id')
            ->join('subcategories', 'teams.subcategory_id', 'subcategories.id')
            ->join('categories', 'subcategories.category_id', 'categories.id')
            ->whereIn('events.id', $ids)
            ->get();
    }

    /**
     * @return array
     */
    public function getFinishedAndWithoutResultsGamesIds(): array
    {
        return DB::table('events')
            ->select('id')
            ->where('finish_game_at', '<', date('Y-m-d H:i:s', time()))
            ->whereNOTIn('id', function ($query) {
                $query->select('event_id')->from('results');
            })
            ->get()
            ->pluck('id')
            ->toArray();
    }
}