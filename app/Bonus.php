<?php

declare (strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Bonus
 *
 * @package App
 * @property int $id
 * @property int $amount
 * @property string|null $description
 * @property int $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Bonus newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Bonus newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Bonus query()
 * @method static \Illuminate\Database\Eloquent\Builder|Bonus whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Bonus whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Bonus whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Bonus whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Bonus whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Bonus whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Bonus extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'amount',
        'description',
        'active',
    ];
}
