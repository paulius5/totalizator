<?php

declare (strict_types=1);

namespace App;

use App\Events\BalanceHistoryCreated;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class BalanceHistory
 *
 * @package App
 * @mixin \Eloquent
 * @property int $id
 * @property int $user_id
 * @property float $amount
 * @property float $balance
 * @property string $sign
 * @property string $message
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|BalanceHistory whereAmount($value)
 * @method static Builder|BalanceHistory whereBalance($value)
 * @method static Builder|BalanceHistory whereCreatedAt($value)
 * @method static Builder|BalanceHistory whereId($value)
 * @method static Builder|BalanceHistory whereMessage($value)
 * @method static Builder|BalanceHistory whereSign($value)
 * @method static Builder|BalanceHistory whereUpdatedAt($value)
 * @method static Builder|BalanceHistory whereUserId($value)
 * @method static Builder|BalanceHistory newModelQuery()
 * @method static Builder|BalanceHistory newQuery()
 * @method static Builder|BalanceHistory query()
 */
class BalanceHistory extends Model
{
    /**
     * @var string
     */
    protected $table = 'balance_history';

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'amount',
        'balance',
        'sign',
        'message',
    ];

    /**
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => BalanceHistoryCreated::class,
    ];
}
