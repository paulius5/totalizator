<?php

declare(strict_types = 1);

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

/**
 * Class BalanceHistoryCreated
 * @package App\Events
 */
class BalanceHistoryCreated implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var $balance
     */
    private $balance;

    /**
     * Create a new event instance.
     *
     * @param $balance
     */
    public function __construct($balance)
    {
        $this->balance = $balance;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('results');
    }

    /**
     * @return array
     */
    public function broadcastWith(): array
    {
        return $this->balance->toArray();
    }
}