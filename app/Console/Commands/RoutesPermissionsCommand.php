<?php

declare(strict_types = 1);

namespace App\Console\Commands;

use App\Permission;
use Illuminate\Console\Command;
use Route;

/**
 * Class RoutesPermissionsCommand
 * @package App\Console\Commands
 */
class RoutesPermissionsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'routes:permissions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create or update routes permissions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(): void
    {
        $routes = Route::getRoutes();

        foreach ($routes as $route) {
            if (in_array(trim($route->getPrefix()?? '', '/'), ['admin', 'company', 'creator', 'player'])) {
                if (isset($route->middleware()[2]) && !in_array($route->getActionMethod(), ['create', 'store', 'show', 'edit', 'update', 'destroy', 'activate', 'deactivate', 'showBet', 'placeBet'])) {
                    list(, $middleware) = explode(':', $route->middleware()[2]);
                    Permission::updateOrCreate(
                        ['route' => $route->uri],
                        ['name' => $middleware]
                    );
                }
            }
        }

        $this->info('Routes permissions command successfully finished!');
    }
}
