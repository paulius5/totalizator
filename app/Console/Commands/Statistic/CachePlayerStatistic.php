<?php

declare(strict_types = 1);

namespace App\Console\Commands\Statistic;

use App\Services\Statistic\PlayerService;
use Illuminate\Console\Command;

/**
 * Class CachePlayerStatistic
 * @package App\Console\Commands\Statistic
 */
class CachePlayerStatistic extends Command
{
    /**
     *
     */
    const TIME_INDEXES = [
        [
            5,          //time interval count (minutes)
            'm',        //time interval name
            '5 minutes' //time interval long name
        ],              //5 minutes interval
        [
            30,
            'm',
            '30 minutes'
        ],              //30 minutes interval
        [
            1,
            'h',
            '1 hour'
        ],              //1 hour interval
        [
            1,
            'd',
            '1 day'
        ],              //1 day interval
        [
            1,
            'mth',
            '1 month'
        ],              //1 month interval
        [
            1,
            'y',
            '1 year'
        ],              //1 year interval
        [
            0,
            'all',
            'all the time'
        ],              //all the time
    ];

    /**
     *
     */
    const BETS_FILTER = [
        [
            'filter_by' => 'Bet placed.',   //database filter
            'key_name' => 'placed',         //cache key additional name
        ],
        [
            'filter_by' => 'Bet win.',
            'key_name' => 'win',
        ],
    ];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'statistic-player:cache
                            {message=Bet placed.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cache player statistic.';

    /**
     * @var PlayerService
     */
    private $playerService;

    /**
     * Create a new command instance.
     *
     * @param PlayerService $playerService
     */
    public function __construct(PlayerService $playerService)
    {
        parent::__construct();
        $this->playerService = $playerService;
    }

    /**
     * Execute the console command.
     *
     * @return void
     * @throws \Exception
     */
    public function handle(): void
    {
        foreach (self::BETS_FILTER as $filter) {
            foreach (self::TIME_INDEXES as $index) {
                $this->playerService->saveStatistic($index[0], $index[1], $filter['filter_by'], $filter['key_name']);
            }
        }
    }
}
