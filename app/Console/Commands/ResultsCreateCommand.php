<?php

declare (strict_types=1);

namespace App\Console\Commands;

use App\Repositories\BalanceHistoryRepository;
use App\Repositories\BetRepository;
use App\Repositories\ResultRepository;
use App\Repositories\UserRepository;
use App\Services\ResultsService;
use Illuminate\Console\Command;

/**
 * Class ResultsCreateCommand
 * @package App\Console\Commands
 */
class ResultsCreateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'results:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create results for finished events and transfer money for bet winnings.';

    /**
     * @var ResultsService
     */
    private $resultsService;
    /**
     * @var ResultRepository
     */
    private $resultRepository;
    /**
     * @var BetRepository
     */
    private $betRepository;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var BalanceHistoryRepository
     */
    private $balanceHistoryRepository;

    /**
     * Create a new command instance.
     *
     * @param ResultsService $resultsService
     * @param ResultRepository $resultRepository
     * @param BetRepository $betRepository
     * @param UserRepository $userRepository
     * @param BalanceHistoryRepository $balanceHistoryRepository
     */
    public function __construct(
        ResultsService $resultsService,
        ResultRepository $resultRepository,
        BetRepository $betRepository,
        UserRepository $userRepository,
        BalanceHistoryRepository $balanceHistoryRepository)
    {
        parent::__construct();
        $this->resultsService = $resultsService;
        $this->resultRepository = $resultRepository;
        $this->betRepository = $betRepository;
        $this->userRepository = $userRepository;
        $this->balanceHistoryRepository = $balanceHistoryRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle(): void
    {
        if (!empty($ids = $this->resultRepository->getFinishedAndWithoutResultsGamesIds())) {
            $events = $this->resultRepository->getCategoriesIdsForFinishedGamesExceptWithResults($ids);

            foreach ($events as $event) {
                $this->resultsService->saveResults($event->id, $event->score_from, $event->score_to);
                $this->info('Created results for event with ID: ' . $event->id);

                $bets = $this->betRepository->getBetsInformationWhichNotClosedByEventId($event->id);

                if (!empty($bets)) {
                    foreach ($bets as $bet) {
                        $bet = (object)$bet;
                        if ($bet->bet_for == 0 && $bet->team1_score == $bet->team2_score ||
                            $bet->bet_for == 1 && $bet->team1_score > $bet->team2_score ||
                            $bet->bet_for == 2 && $bet->team1_score < $bet->team2_score
                        ) {
                            $amount = $bet->bet_rate * $bet->bet_amount;
                            $user = $this->userRepository->find($bet->user_id);

                            $this->userRepository->where('id', $bet->user_id)
                                ->increment('balance', $amount);

                            $this->balanceHistoryRepository->create([
                                'user_id'   => $bet->user_id,
                                'amount'    => $amount,
                                'balance'   => $user->balance,
                                'sign'      => '+',
                                'message'   => 'Bet win.'
                            ]);
                            $this->info('   ' . $user->name . ' won ' . $amount . '€ for this event and bet with ID: ' . $bet->id);
                        }
                        $this->betRepository->update(['closed' => 1], $bet->id);
                    }
                } else {
                    $this->info('   No one betted for this event.');
                }
            }
        } else {
            $this->info('All events have their scores! Nothing to do.');
        }
    }
}
