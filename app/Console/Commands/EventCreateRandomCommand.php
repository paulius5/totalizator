<?php

declare (strict_types=1);

namespace App\Console\Commands;

use App\Services\EventService;

/**
 * Class EventCreateRandomCommand
 * @package App\Console\Commands
 */
class EventCreateRandomCommand extends EventCreateCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature =  'event:create-random 
                            {time? : Time in minutes between open and close bet.}
                            {difference=1 : Difference in minutes between times.}
                            {open=0 : Immediate open event or not.}
                            {finish=1 : Immediate game finish after event closed.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new random event';

    /**
     * Create a new command instance.
     *
     * @param EventService $eventService
     */
    public function __construct(EventService $eventService)
    {
        parent::__construct($eventService);
    }

    /**
     * Execute the console command.
     *
     * @return void
     * @throws \Exception
     */
    public function handle(): void
    {
        $minutes = (int)$this->argument('time');
        $difference = (int)$this->argument('difference');
        $openBetOnCreation = filter_var($this->argument('open'), FILTER_VALIDATE_BOOLEAN);
        $finishGameAfterClose = filter_var($this->argument('finish'), FILTER_VALIDATE_BOOLEAN);

        $subcategoryId = $this->eventService->getRandomSubcategoryIdFromListOrSubcategoriesList();

        if ($subcategoryId) {

            $teamsIds = $this->eventService->get2RandomTeamsIdsFromList($subcategoryId);

            $openTime = 0;
            $closeTime = $openTime + $difference;
            $startTime = $closeTime + $difference;
            $finishTime = $startTime + $difference;
            if (!$openBetOnCreation) {
                $openTime = $minutes;
                $closeTime += $minutes;
                $startTime += $minutes;
                $finishTime += $minutes;
            }

            if ($finishGameAfterClose) {
                $startTime = $closeTime;
                $finishTime = $closeTime;
            }

            if ($minutes > 0) {
                $dateTimeList = $this->setDateAndTimeList(['', '', '', ''], true, $addMinutes = [$openTime, $closeTime, $startTime, $finishTime]);
            } else {
                $dateTimeList = $this->setDateAndTimeList();
            }

            $this->eventService->saveEvent($teamsIds, $dateTimeList);

            $this->info('Random event created successfully!');
        } else {
            $this->error('There are not enough teams with same subcategories!');
        }
    }
}
