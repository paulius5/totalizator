<?php

declare(strict_types = 1);

namespace App\Console\Commands;

use App\BalanceHistory;
use App\Bonus;
use App\Role;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

/**
 * Class UserCreateCommand
 * @package App\Console\Commands
 */
class UserCreateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create user via command line.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(): void
    {
        $roles = Role::all();
        $rolesNames = $roles->pluck('name')->toArray();

        $selectedRoleName = $this->choice('Select role for user', $rolesNames, 0);
        $selectedRoleId = $roles[0]->id;
        foreach ($roles as $role) {
            if ($role->name == $selectedRoleName) {
                $selectedRoleId = $role->id;
            }
        }

        $name = $this->ask('Enter Your name');
        while ($name == null || strlen($name) < 3) {
            $name = $this->ask('Name should be at least 3 symbols. Enter name again.');
        }

        $email = $this->ask('Enter Your email');
        while ($email == null || $this->emailExists($email) || $this->checkEmailFormat($email)) {
            $email = $this->ask('Entered email already exists or invalid! Enter another one.');
        }

        $password = $this->secret('Enter password');
        $passwordConfirmation = $this->secret('Enter password confirmation');
        while ($password != $passwordConfirmation) {
            $passwordConfirmation = $this->secret('Entered password confirmation doesn\'t match password! Enter once again');
        }

        $bonus = Bonus::where('active', 1)->first();

        $data = [
            'name'      => $name,
            'email'     => $email,
            'password'  => Hash::make($password),
        ];

        if ($bonus) {
            array_set($data, 'bonus_id', $bonus->id);
            array_set($data, 'balance', $bonus->amount);
        }

        $user = User::create($data);
        $user->roles()->sync($selectedRoleId);

        if ($bonus) {
            BalanceHistory::create([
                'user_id' => $user->id,
                'amount' => $bonus->amount,
                'balance' => 0,
                'sign' => '+',
                'message' => 'Registration bonus',
            ]);
        }

        $this->info('User successfully created! Now You can login to the system.');
    }

    /**
     * @param string $email
     * @return bool
     */
    private function emailExists(string $email): bool
    {
        return boolval(User::where('email', $email)->get()->toArray());
    }

    /**
     * @param string $email
     * @return bool
     */
    private function checkEmailFormat(string $email): bool
    {
        return !filter_var($email, FILTER_VALIDATE_EMAIL);
    }
}
