<?php

declare (strict_types=1);

namespace App\Console\Commands;

use App\Services\EventService;
use Illuminate\Console\Command;

/**
 * Class EventCreateCommand
 * @package App\Console\Commands
 */
class EventCreateCommand extends Command
{
    const ADD_TIME_UNIT = 'minute';
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'event:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new event';
    /**
     * @var EventService
     */
    protected $eventService;


    /**
     * Create a new command instance.
     *
     * @param EventService $eventService
     */
    public function __construct(EventService $eventService)
    {
        parent::__construct();
        $this->eventService = $eventService;
    }

    /**
     * Execute the console command.
     *
     * @return void
     * @throws \Exception
     */
    public function handle(): void
    {
        $subcategoriesIds = $this->eventService->getRandomSubcategoryIdFromListOrSubcategoriesList(false);

        if ($subcategoriesIds) {

            $categoriesNames = $this->eventService->getCategoriesNamesListBySubcategoriesIds($subcategoriesIds);

            $selectedCategoryName = $this->choice('Select category or hit enter for random selection', $categoriesNames, array_random(array_keys($categoriesNames)));

            $categoryId = $this->eventService->getCategoryIdByName($selectedCategoryName);

            $subcategoriesIds = $this->eventService->getSubcategoriesIdsByCategoryId($subcategoriesIds, $categoryId);
            $subcategoriesNames = $this->eventService->getSubcategoriesNamesByIds($subcategoriesIds);

            $selectedSubcategoryName = $this->choice('Select subcategory or hit enter for random selection', $subcategoriesNames, array_random(array_keys($subcategoriesNames)));
            $selectedSubcategoryId = $this->eventService->getSubcategoryIdByName($selectedSubcategoryName);

            $teams = $this->eventService->getTeamsBySubcategoryId($selectedSubcategoryId);
            $selectedTeam1 = $this->choice('Select team1 or hit enter for random selection', $teams, array_random(array_keys($teams)));

            $teams = $this->eventService->removeArrayItem($teams, $selectedTeam1);
            $selectedTeam2 = $this->choice('Select team2 or hit enter for random selection', $teams, array_random(array_keys($teams)));

            $selectedTeamIds[] = $this->eventService->getTeamIdByName($selectedTeam1);
            $selectedTeamIds[] = $this->eventService->getTeamIdByName($selectedTeam2);

            $useDefaultValues = $this->choice('Set default values for event date and time?', ['Yes', 'No'], 0);

            $dateTimeList = [];
            $defaultDateTime = date('Y-m-d H:i:s', time());
            if ($useDefaultValues == "Yes") {
                $dateTimeList = $this->setDateAndTimeList();
            } else {
                $dateTimeList[] = $this->setDateAndTime('open bet', false, 0, $defaultDateTime);
                $dateTimeList[] = $this->setDateAndTime('close bet', false, 1, $defaultDateTime);
                $dateTimeList[] = $this->setDateAndTime('start game', false, 2, $defaultDateTime);
                $dateTimeList[] = $this->setDateAndTime('finish game', false, 3, $defaultDateTime);
            }

            $useRandomBets = $this->choice('Set random bets for event?', ['Yes', 'No'], 0);

            $bets = [];
            if ($useRandomBets != "Yes") {
                $bets[] = $this->ask('Enter bet for ' . $selectedTeam1 . ' win:', $this->eventService->randomBet());
                $bets[] = $this->ask('Enter bet for ' . $selectedTeam2 . ' win:', $this->eventService->randomBet());
                $bets[] = $this->ask('Enter bet for draw:', $this->eventService->randomBet(6, 10));
            }

            $this->eventService->saveEvent($selectedTeamIds, $dateTimeList, $bets);

            $this->info('Event created successfully!');
        } else {
            $this->error('There are not enough teams with same subcategories!');
        }
    }

    /**
     * @param array $questions
     * @param bool $default
     * @param array $addMinutes
     * @return array
     */
    protected function setDateAndTimeList(array $questions = ['', '', '', ''], bool $default = true, array $addMinutes = [0, 1, 2, 3]): array
    {
        $dateTimeList = [];

        $dateTimeList[] = $this->setDateAndTime($questions[0], $default, $addMinutes[0]);
        $dateTimeList[] = $this->setDateAndTime($questions[1], $default, $addMinutes[1]);
        $dateTimeList[] = $this->setDateAndTime($questions[2], $default, $addMinutes[2]);
        $dateTimeList[] = $this->setDateAndTime($questions[3], $default, $addMinutes[3]);

        return $dateTimeList;
    }

    /**
     * @param string $question
     * @param bool $default
     * @param int $addAmount
     * @param string $defaultDateTime
     * @return string
     */
    private function setDateAndTime(string $question = '', bool $default = true, int $addAmount = 0, string $defaultDateTime = ''): string
    {
        if (!$defaultDateTime) {
            $defaultDateTime = date('Y-m-d H:i:s');
        }
        if (!$default) {
            $date = $this->ask('Enter ' . $question . ' date', date('Y-m-d', strtotime($defaultDateTime . $addAmount . self::ADD_TIME_UNIT)));
            $time = $this->ask('Enter ' . $question . ' time', date('H:i', strtotime($defaultDateTime . $addAmount . self::ADD_TIME_UNIT)));
        } else {
            list($date, $time) = explode(' ', date('Y-m-d H:i:s', strtotime($defaultDateTime . $addAmount . self::ADD_TIME_UNIT)));
        }
        return strtr(':date :time', [
            ':date' => $date,
            ':time' => $time,
        ]);
    }
}
