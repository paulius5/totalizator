<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     * @throws \Exception
     */
    protected function schedule(Schedule $schedule)
    {
         $schedule->command('event:create-random', [
             random_int(1, 10),
             random_int(1, 10),
             random_int(0, 1),
             random_int(0, 1),
         ])->everyMinute();
         $schedule->command('results:create')->everyMinute();
         $schedule->command('statistic-player:cache')->everyFiveMinutes();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
