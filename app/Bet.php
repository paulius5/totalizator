<?php

declare (strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Bet
 *
 * @property-read \App\Event $event
 * @mixin \Eloquent
 * @property-read \App\User $user
 * @property int $id
 * @property int $user_id
 * @property int $event_id
 * @property int $bet_for
 * @property float $bet_rate
 * @property float $bet_amount
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int $closed
 * @method static Builder|Bet whereBetAmount($value)
 * @method static Builder|Bet whereBetFor($value)
 * @method static Builder|Bet whereBetRate($value)
 * @method static Builder|Bet whereClosed($value)
 * @method static Builder|Bet whereCreatedAt($value)
 * @method static Builder|Bet whereEventId($value)
 * @method static Builder|Bet whereId($value)
 * @method static Builder|Bet whereUpdatedAt($value)
 * @method static Builder|Bet whereUserId($value)
 * @method static Builder|Bet newModelQuery()
 * @method static Builder|Bet newQuery()
 * @method static Builder|Bet query()
 */
class Bet extends Model
{
    protected $fillable = [
        'user_id',
        'event_id',
        'bet_for',
        'bet_rate',
        'bet_amount',
        'closed',
    ];

    /**
     * @return BelongsTo
     */
    public function event(): BelongsTo
    {
        return $this->belongsTo(Event::class);
    }

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
