<?php

declare (strict_types=1);

namespace App;

use App\Events\EventCreated;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Event
 *
 * @property-read Team $team1
 * @property-read Team $team2
 * @mixin \Eloquent
 * @property-read \App\Result $results
 * @property int $id
 * @property int $team1_id
 * @property int $team2_id
 * @property string $open_bet_at
 * @property string $close_bet_at
 * @property string $start_game_at
 * @property string $finish_game_at
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property float $team1_win_rate
 * @property float $team2_win_rate
 * @property float $draw_rate
 * @method static Builder|Event whereCloseBetAt($value)
 * @method static Builder|Event whereCreatedAt($value)
 * @method static Builder|Event whereDrawRate($value)
 * @method static Builder|Event whereFinishGameAt($value)
 * @method static Builder|Event whereId($value)
 * @method static Builder|Event whereOpenBetAt($value)
 * @method static Builder|Event whereStartGameAt($value)
 * @method static Builder|Event whereTeam1Id($value)
 * @method static Builder|Event whereTeam1WinRate($value)
 * @method static Builder|Event whereTeam2Id($value)
 * @method static Builder|Event whereTeam2WinRate($value)
 * @method static Builder|Event whereUpdatedAt($value)
 * @method static Builder|Event newModelQuery()
 * @method static Builder|Event newQuery()
 * @method static Builder|Event query()
 */
class Event extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'team1_id',
        'team2_id',
        'open_bet_at',
        'close_bet_at',
        'start_game_at',
        'finish_game_at',
        'created_at',
        'updated_at',
        'team1_win_rate',
        'team2_win_rate',
        'draw_rate',
    ];

    /**
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => EventCreated::class,
    ];

    /**
     * @return BelongsTo
     */
    public function team1(): BelongsTo
    {
        return $this->belongsTo(Team::class, 'team1_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function team2(): BelongsTo
    {
        return $this->belongsTo(Team::class, 'team2_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function results(): BelongsTo
    {
        return $this->belongsTo(Result::class, 'id', 'event_id');
    }
}
