<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Result
 *
 * @package App
 * @mixin \Eloquent
 * @property int $id
 * @property int $event_id
 * @property int $team1_score
 * @property int $team2_score
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Result whereCreatedAt($value)
 * @method static Builder|Result whereEventId($value)
 * @method static Builder|Result whereId($value)
 * @method static Builder|Result whereTeam1Score($value)
 * @method static Builder|Result whereTeam2Score($value)
 * @method static Builder|Result whereUpdatedAt($value)
 * @method static Builder|Result newModelQuery()
 * @method static Builder|Result newQuery()
 * @method static Builder|Result query()
 */
class Result extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'event_id',
        'team1_score',
        'team2_score',
    ];
}
