<?php

declare (strict_types=1);

namespace App\Services;

use App\Repositories\ResultRepository;

/**
 * Class ResultsService
 * @package App\Services
 */
class ResultsService
{
    /**
     * @var ResultRepository
     */
    private $resultRepository;

    /**
     * ResultsService constructor.
     * @param ResultRepository $resultRepository
     */
    public function __construct(ResultRepository $resultRepository)
    {
        $this->resultRepository = $resultRepository;
    }

    /**
     * @param $id
     * @param $score_from
     * @param $score_to
     * @throws \Exception
     */
    public function saveResults($id, $score_from, $score_to)
    {
        $this->resultRepository->create([
            'event_id' => $id,
            'team1_score' => mt_rand($score_from, $score_to),
            'team2_score' => mt_rand($score_from, $score_to),
        ]);
    }
}