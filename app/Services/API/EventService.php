<?php

declare(strict_types = 1);

namespace App\Services\API;

use App\Repositories\EventRepository;
use Illuminate\Support\Collection;

/**
 * Class EventService
 * @package App\Services\API
 */
class EventService
{
    /**
     * @var EventRepository
     */
    private $eventRepository;

    /**
     * EventService constructor.
     * @param $eventRepository
     */
    public function __construct(EventRepository $eventRepository)
    {
        $this->eventRepository = $eventRepository;
    }

    /**
     * @return Collection
     */
    public function availableEvents(): Collection
    {
        return $this->eventRepository->getAvailableEvents();
    }
}