<?php

declare(strict_types = 1);

namespace App\Services\API;

use App\Repositories\CategoryRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class CategoryService
 * @package App\Services\API
 */
class CategoryService
{
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * CategoryService constructor.
     * @param CategoryRepository $categoryRepository
     */
    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @return Builder[]|Collection
     * @throws \Exception
     */
    public function categoriesWithSubcategories()
    {
        return $this->categoryRepository->with(['subcategories'])->get();
    }
}