<?php

declare(strict_types = 1);

namespace App\Services\Statistic;

use App\Repositories\BalanceHistoryRepository;
use App\Repositories\UserRepository;
use Cache;
use Carbon\Carbon;

/**
 * Class PlayerService
 * @package App\Services\Statistic
 */
class PlayerService
{
    /** @var BalanceHistoryRepository */
    private $balanceHistoryRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * PlayerService constructor.
     * @param BalanceHistoryRepository $balanceHistoryRepository
     * @param UserRepository $userRepository
     */
    public function __construct(BalanceHistoryRepository $balanceHistoryRepository, UserRepository $userRepository)
    {
        $this->balanceHistoryRepository = $balanceHistoryRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @param string $message
     * @param int $count
     * @param string $timeIndex
     * @return array
     * @throws \Exception
     */
    public function getStatistic(int $count = 5, string $timeIndex = 'm', string $message = 'Bet placed.'): array
    {
        switch ($timeIndex) {
            case 'm':
                $date = Carbon::now()->subMinutes($count);
                break;
            case 'h':
                $date = Carbon::now()->subHours($count);
                break;
            case 'd':
                $date = Carbon::now()->subDays($count);
                break;
            case 'mth':
                $date = Carbon::now()->subMonths($count);
                break;
            case 'y':
                $date = Carbon::now()->subYears($count);
                break;
            default:
                $date = '1970-01-01 00:00:00';
        }
        return $this->balanceHistoryRepository->makeQuery()
            ->selectRaw('COUNT(amount) as count, SUM(amount) as sum, user_id')
            ->where('message', $message)
            ->where('created_at', '>=', $date)
            ->groupBy('user_id')
            ->get()
            ->toArray();
    }

    /**
     * @param int $count
     * @param string $timeIndex
     * @param string $message
     * @param string $extraKeyValue
     * @return void
     * @throws \Exception
     */
    public function saveStatistic(int $count = 5, string $timeIndex = 'm', string $message = 'Bet placed.', string $extraKeyValue = ''): void
    {
        $this->clearStatistic(strtr(':dateIndex_:extra', [
            ':dateIndex' => "$count$timeIndex",
            ':extra' => $extraKeyValue
        ]));

        foreach ($this->getStatistic($count, $timeIndex, $message) as $row) {
            Cache::tags('user_' . $row['user_id'])->forever(strtr(':dateIndex_:extra', [
                ':dateIndex' => "$count$timeIndex",
                ':extra' => $extraKeyValue,
            ]), array_only($row, ['count', 'sum']));
        }
    }

    /**@return void
     * @param string $cacheKey
     * @throws \Exception
     */
    public function clearStatistic(string $cacheKey): void
    {
        foreach ($this->getAllUsersIds() as $id)
        {
            Cache::tags('user_' . $id)->forever($cacheKey, [
                'count' => 0,
                'sum' => 0,
            ]);
        }
    }

    /**
     * @return array|null
     * @throws \Exception
     */
    public function getAllUsersIds(): ?array
    {
        return $this->userRepository->all()->pluck('id')->toArray();
    }
}