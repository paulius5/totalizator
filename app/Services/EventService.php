<?php

declare (strict_types=1);

namespace App\Services;

use App\Event;
use App\Repositories\EventRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 * Class EventService
 * @package App\Services
 */
class EventService
{
    /**
     * @var EventRepository
     */
    private $eventRepository;

    /**
     * EventService constructor.
     * @param EventRepository $eventRepository
     */
    public function __construct(EventRepository $eventRepository)
    {
        $this->eventRepository = $eventRepository;
    }

    /**
     * Get random id from the subcategories list
     *
     * @param bool $random
     * @param int $amount
     * @return array
     */
    public function getRandomSubcategoryIdFromListOrSubcategoriesList(bool $random = true, int $amount = 1): array
    {
        $ids = $this->eventRepository->getSubcategoriesIdsWhichHave2OrMoreTeams();

        if($ids->count() > 0) {
            if ($random) {
                return $ids->shuffle()->random($amount)->toArray();
            } else {
                return $ids->toArray();
            }
        } else {
            return [];
        }
    }

    /**
     * @param $id
     * @return array
     */
    public function get2RandomTeamsIdsFromList($id): array
    {
        return $this->getTeamsIdsBySubcategoryId($id)->shuffle()->random(2)->pluck('id')->toArray();
    }

    /**
     * @param $subcategoriesIds
     * @return array
     */
    public function getCategoriesNamesListBySubcategoriesIds($subcategoriesIds): array
    {
        $ids = $this->getCategoriesIdsBySubcategoriesIds($subcategoriesIds);

        return $this->getCategoriesNamesByIds($ids)->toArray();
    }

    /**
     * @param $list
     * @param $value
     * @return array|null
     */
    public function removeArrayItem($list, $value): ?array
    {
        if (($key = array_search($value, $list)) !== false) {
            unset($list[$key]);
        }

        return $list;
    }

    /**
     * @param $teamsIds
     * @param $dateTimeList
     * @param array $bets
     * @return Event
     * @throws \Exception
     */
    public function saveEvent($teamsIds, $dateTimeList, array $bets = []): Model
    {
        if (!$bets) {
            $bets = $this->randomBets();
        }

        return $this->eventRepository->create([
            'team1_id' => $teamsIds[0],
            'team2_id' => $teamsIds[1],
            'open_bet_at' => $dateTimeList[0],
            'close_bet_at' => $dateTimeList[1],
            'start_game_at' => $dateTimeList[2],
            'finish_game_at' => $dateTimeList[3],
            'created_at' => date("Y-m-d H:i:s"),
            'update_at' => date("Y-m-d H:i:s"),
            'team1_win_rate' => $bets[0],
            'team2_win_rate' => $bets[1],
            'draw_rate' => $bets[2],
        ]);
    }

    /**
     * @return array
     */
    public function randomBets(): array
    {
        return [
            round(mt_rand() / mt_getrandmax() + mt_rand(1, 5), 5),
            round(mt_rand() / mt_getrandmax() + mt_rand(1, 5), 5),
            round(mt_rand() / mt_getrandmax() + mt_rand(6, 10), 5),
        ];
    }

    /**
     * @param int $min
     * @param int $max
     * @return float
     */
    public function randomBet(int $min = 1, int $max = 5): float
    {
        return round(mt_rand() / mt_getrandmax() + mt_rand($min, $max), 5);
    }

    /**
     * @param $name
     * @return int
     */
    public function getTeamIdByName($name): int
    {
        return DB::table('teams')
            ->select('id')
            ->where('name', $name)
            ->get()
            ->pluck('id')
            ->toArray()[0];
    }

    /**
     * @param $id
     * @return array
     */
    public function getTeamsBySubcategoryId($id): array
    {
        return DB::table('teams')
            ->select('name')
            ->where('subcategory_id', $id)
            ->get()
            ->pluck('name')
            ->toArray();
    }

    /**
     * @param $name
     * @return int
     */
    public function getSubcategoryIdByName($name): int
    {
        return DB::table('subcategories')
            ->select('id')
            ->where('name', $name)
            ->get()
            ->pluck('id')
            ->toArray()[0];
    }

    /**
     * @param $ids
     * @return array
     */
    public function getSubcategoriesNamesByIds($ids): array
    {
        return DB::table('subcategories')
            ->select('name')
            ->whereIn('id', $ids)
            ->get()
            ->pluck('name')
            ->toArray();
    }

    /**
     * @param $ids
     * @param $category_id
     * @return array
     */
    public function getSubcategoriesIdsByCategoryId($ids, $category_id): array
    {
        return DB::table('subcategories')
            ->select('id')
            ->where('category_id', $category_id)
            ->whereIn('id', $ids)
            ->get()
            ->pluck('id')
            ->toArray();
    }

    /**
     * @param $name
     * @return int
     */
    public function getCategoryIdByName($name): int
    {
        return DB::table('categories')
            ->select('id')
            ->where('name', $name)
            ->get()
            ->pluck('id')[0];
    }

    /**
     * @param $ids
     * @return Collection
     */
    public function getCategoriesNamesByIds($ids): Collection
    {
        return DB::table('categories')
            ->select('name')
            ->whereIn('id', $ids)
            ->get()
            ->pluck('name');
    }

    /**
     * @param $ids
     * @return Collection
     */
    public function getCategoriesIdsBySubcategoriesIds($ids): Collection
    {
        return Db::table('subcategories')
            ->select(['category_id'])
            ->distinct()
            ->whereIn('id', $ids)
            ->get()
            ->pluck('category_id');
    }

    /**
     * @param $id
     * @return Collection
     */
    public function getTeamsIdsBySubcategoryId($id): Collection
    {
        return DB::table('teams')
            ->select('id')
            ->where('subcategory_id', $id)
            ->get();
    }

    /**
     * @param int $eventId
     * @param int $selection
     * @return float
     * @throws \Exception
     */
    public function getRate(int $eventId, int $selection): float
    {
        $event = $this->eventRepository->where('id', $eventId)->get();

        if ($selection == 1){
            return (float)$event->pluck('team1_win_rate')->toArray()[0];
        } elseif ($selection == 2) {
            return (float)$event->pluck('team2_win_rate')->toArray()[0];
        } else {
            return (float)$event->pluck('draw_rate')->toArray()[0];
        }
    }

    /**
     * @param int $eventId
     * @return bool
     * @throws \Exception
     */
    public function eventClosed(int $eventId): bool
    {
        $dateTime = $this->eventRepository->where('id', $eventId)->pluck('close_bet_at')->toArray();

        if (!$dateTime) {
            return false;
        }

        return strtotime($dateTime[0]) < time();
    }
}