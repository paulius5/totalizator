<?php

declare (strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Team
 *
 * @package App
 * @property-read Category $category
 * @property-read Subcategory $subcategory
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $subcategory_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static Builder|Team whereCreatedAt($value)
 * @method static Builder|Team whereDescription($value)
 * @method static Builder|Team whereId($value)
 * @method static Builder|Team whereName($value)
 * @method static Builder|Team whereSubcategoryId($value)
 * @method static Builder|Team whereUpdatedAt($value)
 * @method static Builder|Team newModelQuery()
 * @method static Builder|Team newQuery()
 * @method static Builder|Team query()
 */
class Team extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'subcategory_id',
    ];

    /**
     * @return BelongsTo
     */
    public function subcategory(): BelongsTo
    {
        return $this->belongsTo(Subcategory::class);
    }

    /**
     * @return BelongsTo
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class, 'subcategory_id', 'id');
    }
}
