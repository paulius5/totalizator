import Vue from 'vue'
import Vuex from 'vuex'
import VueTimers from 'vue-timers'

Vue.use(Vuex);
Vue.use(VueTimers);

export default new Vuex.Store({
    state: {
        events: [],
        categories: [],
        categoryId: null,
        subCategoryId: null,
        isLoading: false,
        user: null,
    },
    getters: {

    },
    mutations: {
        /* Events methods */
        setEvents(state, events) {
            state.events = events;
        },
        deleteEvent(state, event) {
            state.events.splice(state.events.indexOf(event), 1);
        },
        addEvent(state, event) {
            state.events.push(event);
        },
        /* Categories methods */
        setCategories(state, categories) {
            state.categories = categories;
        },
        /* Loading methods */
        startLoading(state) {
            state.isLoading = true;
        },
        stopLoading(state) {
            state.isLoading = false;
        },
        /* User methods */
        setUser(state, user) {
            state.user = user;
        },
    },
    actions: {
        getEvents({ commit }) {
            commit('startLoading');
            fetch(route('api.available.events'))
                .then(function(response) {
                    return response.json();
                })
                .then(function(response) {
                    commit('setEvents', response);
                    commit('stopLoading');
                });

        },
        getCategories({ commit }) {
            fetch(route('api.categories'))
                .then(function(response) {
                    return response.json();
                })
                .then(function(response) {
                    commit('setCategories', response);
                });
        },
        getUser({ commit }) {
            fetch(route('session.user'))
                .then(function(response) {
                    return response.json();
                })
                .then(function(response) {
                    commit('setUser', response);
                });
        }
    }
})
