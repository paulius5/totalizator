
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.config.productionTip = true;

import AppEvents from './components/AppEvents';
import store from './store';

if (document.getElementById('events-list')) {
    new Vue({
        store,
        render: h => h(AppEvents),
    }).$mount('#events-list');
}

import AppNotification from './components/AppNotification';

new Vue({
    store,
    render: h => h(AppNotification),
}).$mount('#notification');
