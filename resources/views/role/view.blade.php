@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Role View: {{ $role->Name }}
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <table class="table">
                            <tr>
                                <td>{{ __('ID') }}</td>
                                <td>{{ $role->id }}</td>
                            </tr>
                            <tr>
                                <td>{{ __('Name') }}</td>
                                <td>{{ $role->name }}</td>
                            </tr>
                            <tr>
                                <td>{{ __('Description') }}</td>
                                <td>{{ $role->description }}</td>
                            </tr>
                            <tr>
                                <td>{{ __('Default?') }}</td>
                                <td>{{ $role->default? "Default" : "Not default" }}</td>
                            </tr>
                            <tr>
                                <td>{{ __('Full access?') }}</td>
                                <td>{{ $role->active? "Yes, all control" : "No" }}</td>
                            </tr>
                            <tr>
                                <td>{{ __('Active?') }}</td>
                                <td>{{ $role->active? "Active" : "Inactive" }}</td>
                            </tr>
                            <tr>
                                <td>{{ __('Created at') }}</td>
                                <td>{{ $role->created_at }}</td>
                            </tr>
                            <tr>
                                <td>{{ __('Updated at') }}</td>
                                <td>{{ $role->updated_at }}</td>
                            </tr>
                        </table>

                        <a class='btn btn-secondary btn-sm' href={{ route('role.index') }}>Back to list</a>
                        <a class='btn btn-success btn-sm' href={{ route('role.edit', [$role->id]) }}>Edit</a>
                        <form action="{{ route('role.destroy', [$role->id]) }}" method="post" class="d-inline-block" onsubmit="return confirm('Do you really want to delete?');">
                            @method('delete')
                            @csrf
                            <input class="btn btn-danger btn-sm" type="submit" value="Delete">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection