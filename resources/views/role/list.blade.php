@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Roles List
                        <a class="btn btn-sm btn-primary" href="{{ route('role.create') }}">{{ __('New') }}</a>
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        @if (session('error'))
                            <div class="alert alert-danger" role="alert">
                                {{ session('error') }}
                            </div>
                        @endif

                        @if (count($roles) > 0)
                            <table class="table">
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Default?</th>
                                    <th>Full access?</th>
                                    <th>Active?</th>
                                    <th>Actions</th>
                                </tr>
                                @foreach($roles as $role)
                                    <tr>
                                        <td>{{ $role->id }}</td>
                                        <td>
                                            <a href="{{ route('role.show', [$role->id]) }}">
                                                {{ $role->name }}
                                            </a>
                                        </td>
                                        <td>{{ $role->description }}</td>
                                        <td class="{{ ($role->default)? "text-success": "text-danger" }}">{{ ($role->default)? "Yes": "No" }}</td>
                                        <td class="{{ ($role->full_access)? "text-success": "text-danger" }}">{{ ($role->full_access)? "Yes": "No" }}</td>
                                        <td class="{{ ($role->active)? "text-success": "text-danger" }}">{{ ($role->active)? "Active": "Inactive" }}</td>
                                        <td>
                                            <a class='btn btn-sm btn-info'
                                               href="{{ route('role.show', [$role->id]) }}">{{ __('View') }}</a>
                                            <a class='btn btn-sm btn-success'
                                               href="{{ route('role.edit', [$role->id]) }}">{{ __('Edit') }}</a>
                                            <form action="{{ route('role.destroy', [$role->id]) }}" method="post"
                                                  class="d-inline-block"
                                                  onsubmit="return confirm('Do you really want to delete?');">
                                                @method('delete')
                                                @csrf
                                                <input class="btn btn-sm btn-outline-danger" type="submit" value="Delete">
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                            {{ $roles->links() }}
                        @else
                            There are no roles yet!
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
