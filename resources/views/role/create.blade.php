@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Role New
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form action="{{ route('role.store') }}" method="post">

                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="name">{{ __('Name') }}:</label>
                                <input id="name" class="form-control" type="text" name="name"
                                       value="{{ old('name') }}">
                                @if($errors->has('name'))
                                    <div class="alert-danger">{{ $errors->first('name') }}</div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="description">{{ __('Description') }}:</label>
                                <input id="description" class="form-control" type="text" name="description"
                                       value="{{ old('description') }}">
                                @if($errors->has('description'))
                                    <div class="alert-danger">{{ $errors->first('description') }}</div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="default">{{ __('Default?') }}</label>
                                <input type="hidden" name="default" value="0">
                                <input id="default" type="checkbox" name="default" value="1" {{ old('default')? "checked" : "" }}>
                            </div>

                            <div class="form-group">
                                <label for="full_access">{{ __('Full access?') }}</label>
                                <input type="hidden" name="full_access" value="0">
                                <input id="full_access" type="checkbox" name="full_access" value="1" onclick="togglePermissions()" {{ old('full_access')? "checked" : "" }}>
                            </div>

                            <div class="form-group">
                                <label for="active">{{ __('Active?') }}</label>
                                <input type="hidden" name="active" value="0">
                                <input id="active" type="checkbox" name="active" value="1" {{ old('active')? "checked" : "" }}>
                            </div>

                            <div class="form-group" id="permissions">
                                <div>
                                    <label>{{ __('Available permissions') }}:</label>
                                </div>
                                <div class="container-fluid">
                                    <div class="row">
                                        @foreach($permissions as $permission)
                                            <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                                                <label>
                                                    <input type="checkbox" name="permissions[]" value="{{ $permission->id }}" {{ (in_array($permission->id, old('permissions', []))? 'checked': '') }}> {{ ucfirst($permission->name) }}
                                                </label>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <input class="btn btn-success btn-sm" type="submit" value="{{ __('Save') }}">
                                <a class='btn btn-secondary btn-sm' href={{ route('role.index') }}>Back to list</a>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function togglePermissions()
        {
            let checkToggle = document.getElementById('full_access').checked;
            let permissionsBlock = document.getElementById('permissions');
            if (checkToggle) {
                permissionsBlock.classList.add('d-none');
            } else {
                permissionsBlock.classList.remove('d-none');
            }
        }
    </script>
@endsection