@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        User Edit
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form action="{{ route('user.update', [$user->id]) }}" method="post">

                            {{ method_field('put') }}
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="name">{{ __('Name') }}:</label>
                                <input id="name" class="form-control" type="text" name="name"
                                       value="{{ old('name', $user->name) }}">
                                @if($errors->has('name'))
                                    <div class="alert-danger">{{ $errors->first('name') }}</div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="description">{{ __('Email') }}:</label>
                                <input id="email" class="form-control" type="text" name="email"
                                       value="{{ old('email', $user->email) }}">
                                @if($errors->has('email'))
                                    <div class="alert-danger">{{ $errors->first('email') }}</div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label>{{ __('Roles') }}:</label>
                                @foreach($roles as $role)
                                    <br>
                                    <label for="role_{{ $role->id }}">
                                        <input type="checkbox" name="roles[]" id="role_{{ $role->id }}"
                                               value="{{ $role->id }}"
                                                {{ in_array($role->id, old('role', $user->roles->pluck('id')->toArray()))? 'checked' : '' }}> {{ __($role->name) }}
                                    </label>
                                @endforeach
                                @if($errors->has('role'))
                                    <div class="alert-danger">{{ $errors->first('role') }}</div>
                                @endif
                            </div>

                            <div class="form-group">
                                <input class="btn btn-success btn-sm" type="submit" value="{{ __('Save') }}">
                                <a class='btn btn-secondary btn-sm' href={{ route('user.index') }}>Back to list</a>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection