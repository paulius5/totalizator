@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        User Info
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <table class="table">
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Registration date</th>
                                <th>Balance</th>
                                <th>Roles</th>
                            </tr>
                            <tr>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->created_at }}</td>
                                <td>{{ $user->balance }}&euro;</td>
                                <td>
                                    @forelse($user->roles as $role)
                                        {{ $role->name }}
                                        <strong>
                                            @if ($role->active)
                                                (Active)
                                            @else
                                                (Inactive)
                                            @endif
                                        </strong>
                                        <br>
                                    @empty
                                        No roles yet!
                                    @endforelse
                                </td>
                            </tr>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
