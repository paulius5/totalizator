@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        @if (isset($user->name))
                            User View: {{ $user->Name }}
                        @endif
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        @if (session('error'))
                            <div class="alert alert-danger" role="alert">
                                {{ session('error') }}
                            </div>
                        @endif

                        <table class="table">
                            <tr>
                                <td>{{ __('ID') }}</td>
                                <td>{{ $user->id }}</td>
                            </tr>
                            <tr>
                                <td>{{ __('Name') }}</td>
                                <td>{{ $user->name }}</td>
                            </tr>
                            <tr>
                                <td>{{ __('Email') }}</td>
                                <td>{{ $user->email }}</td>
                            </tr>
                            <tr>
                                <td>{{ __('Roles') }}</td>
                                <td>
                                    @forelse($user->roles as $role)
                                        {{ $role->name }}<br>
                                    @empty
                                        <strong>User don't have any roles yet!</strong>
                                    @endforelse
                                </td>
                            </tr>
                            <tr>
                                <td>{{ __('Created at') }}</td>
                                <td>{{ $user->created_at }}</td>
                            </tr>
                            <tr>
                                <td>{{ __('Updated at') }}</td>
                                <td>{{ $user->updated_at }}</td>
                            </tr>
                        </table>

                        <a class='btn btn-secondary btn-sm' href={{ route('user.index') }}>Back to list</a>
                        <a class='btn btn-success btn-sm' href={{ route('user.edit', [$user->id]) }}>Edit</a>
                        <form action="{{ route('user.destroy', [$user->id]) }}" method="post" class="d-inline-block"
                              onsubmit="return confirm('Do you really want to delete?');">
                            @method('delete')
                            @csrf
                            <input class="btn btn-danger btn-sm" type="submit" value="Delete">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection