@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-10 col-lg-8">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                <div class="card">
                    <div class="card-header text-center font-weight-bold">
                        Welcome
                    </div>
                    <div class="card-body">
                        <h2 class="text-success font-weight-bold text-center">Enjoy TOTALIZATOR game for FREE!</h2>
                    </div>
                    <div class="card-footer text-center">
                        <h3 class="text-info font-weight-bold">
                            @guest
                                @if ($bonus)
                                    <div>Register and get <span class="text-success">{{ $bonus->amount }}&euro;</span> for betting!</div>
                                @else
                                    <div>Register to get started. No bonuses for registration at the moment.</div>
                                @endif
                            @else
                                <div>Spend money for betting and win more money.</div>
                            @endguest
                        </h3>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header text-center font-weight-bold">
                        Bet for:
                    </div>
                    <div class="card-body">
                        <a href="{{ route('player.available.events') }}">
                            <table class="table random-events text-center">
                                @foreach($events as $event)
                                    <tr>
                                        <td class="team1-bet">{{ $event->team1->name }} {{ $event->team1_win_rate }}</td>
                                        <td class="draw-bet">X&nbsp;{{ $event->draw_rate }}</td>
                                        <td class="team2-bet">{{ $event->team2->name }} {{ $event->team2_win_rate }}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection