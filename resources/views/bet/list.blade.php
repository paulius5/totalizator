@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        @if (request()->route()->getActionMethod() == "companyOpenBets")
                            Open Bets List
                        @elseif (request()->route()->getActionMethod() == "companyClosedBets")
                            Closed Bets List
                        @elseif (request()->route()->getActionMethod() == "playerOpenBets")
                            My open Bets List
                        @elseif (request()->route()->getActionMethod() == "playerClosedBets")
                            My closed Bets List
                        @else
                            All Bets List
                        @endif
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <table class="table">
                            <tr>
                                <th>ID</th>
                                @if (request()->route()->getActionMethod() != "playerOpenBets" && request()->route()->getActionMethod() != "playerClosedBets")
                                    <th>User</th>
                                @endif
                                <th>Team1</th>
                                <th>Team2</th>
                                <th>Bet for</th>
                                <th>Bet rate</th>
                                <th>Bet amount</th>
                                <th>Possible win</th>
                                <th>Open/closed</th>
                            </tr>

                            @foreach($bets as $bet)
                                @if (request()->route()->getActionMethod() == "playerClosedBets" ||
                                request()->route()->getActionMethod() == "companyClosedBets" ||
                                (request()->route()->getActionMethod() == "index" && $bet->closed))

                                    @if ($bet->bet_for == 0 && $bet->event->results->team1_score == $bet->event->results->team2_score ||
                                    $bet->bet_for == 1 && $bet->event->results->team1_score > $bet->event->results->team2_score ||
                                    $bet->bet_for == 2 && $bet->event->results->team1_score < $bet->event->results->team2_score)
                                        <tr class="text-success font-weight-bold">
                                    @else
                                        <tr class="text-danger font-weight-bold">
                                    @endif
                                @else
                                    <tr>
                                @endif

                                    <td>{{ $bet->id }}</td>
                                    @if (request()->route()->getActionMethod() != "playerOpenBets" && request()->route()->getActionMethod() != "playerClosedBets")
                                        <td>{{ $bet->user->name }}</td>
                                    @endif
                                    <td>{{ $bet->event->team1->name }}</td>
                                    <td>{{ $bet->event->team2->name }}</td>
                                    <td>
                                        @if ($bet->bet_for == 1)
                                            {{ $bet->event->team1->name }} {{ __('wins') }}
                                        @elseif ($bet->bet_for == 2)
                                            {{ $bet->event->team2->name }} {{ __('wins') }}
                                        @else
                                            {{ __('Draw') }}
                                        @endif
                                    </td>
                                    <td>{{ $bet->bet_rate }}</td>
                                    <td>{{ $bet->bet_amount }}</td>
                                    <td>{{ round($bet->bet_rate * $bet->bet_amount, 2) }}</td>
                                    <td>
                                        @if ($bet->closed)
                                            {{__('Closed')}}
                                        @else
                                            {{__('Open')}}
                                        @endif
                                    </td>
                                </tr>
                            @endforeach

                        </table>
                        {{ $bets->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
