@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Bonuses List
                        <a class="btn btn-sm btn-primary" href="{{ route('bonus.create') }}">{{ __('New') }}</a>
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        @if (session('error'))
                            <div class="alert alert-danger" role="alert">
                                {{ session('error') }}
                            </div>
                        @endif

                        @if (count($bonuses) > 0)
                            <table class="table">
                                <tr>
                                    <th>ID</th>
                                    <th>Amount</th>
                                    <th>Description</th>
                                    <th>Active</th>
                                    <th>Actions</th>
                                </tr>
                                @foreach($bonuses as $bonus)
                                    <tr>
                                        <td>{{ $bonus->id }}</td>
                                        <td>{{ $bonus->amount }}</td>
                                        <td>{{ $bonus->description }}</td>
                                        <td>{{ ($bonus->active)? "Active": "Inactive" }}</td>
                                        <td>
                                            @if (!$bonus->active)
                                                <form action="{{ route('bonus.activate', [$bonus->id]) }}"
                                                      method="post">
                                                    @csrf
                                                    <input class="btn btn-sm btn-success" type="submit"
                                                           value="Activate">
                                                </form>
                                            @else
                                                <form action="{{ route('bonus.deactivate', [$bonus->id]) }}"
                                                      method="post">
                                                    @csrf
                                                    <input class="btn btn-sm btn-danger" type="submit"
                                                           value="Deactivate">
                                                </form>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                            {{ $bonuses->links() }}
                        @else
                            There are no bonuses yet!
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
