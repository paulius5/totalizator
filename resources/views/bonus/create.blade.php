@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Bonus New
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form action="{{ route('bonus.store') }}" method="post">

                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="amount">{{ __('Amount') }}:</label>
                                <input id="amount" class="form-control" type="number" name="amount"
                                       value="{{ old('amount') }}">
                                @if($errors->has('amount'))
                                    <div class="alert-danger">{{ $errors->first('amount') }}</div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="description">{{ __('Description') }}:</label>
                                <input id="description" class="form-control" type="text" name="description"
                                       value="{{ old('description') }}">
                                @if($errors->has('description'))
                                    <div class="alert-danger">{{ $errors->first('description') }}</div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="active">{{ __('Active?') }}</label>
                                <input id="active" type="checkbox" name="active" value="1" {{ old('active')? "checked" : "" }}>
                            </div>

                            <div class="form-group">
                                <input class="btn btn-success btn-sm" type="submit" value="{{ __('Save') }}">
                                <a class='btn btn-secondary btn-sm' href={{ route('bonus.index') }}>Back to list</a>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection