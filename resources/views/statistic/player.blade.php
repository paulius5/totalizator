@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Bets statistic <span class="text-warning">(Statistic is updating every 5 minutes)</span>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            @if ($stats)
                                @foreach(\App\Console\Commands\Statistic\CachePlayerStatistic::BETS_FILTER as $filter)
                                    @isset($stats[$filter['key_name']])
                                        <div class="col-6">
                                            <table class="table table-sm">
                                                <tr>
                                                    <th>Time interval</th>
                                                    <th>Bets {{ $filter['key_name'] }}</th>
                                                    <th>Sum of bets</th>
                                                </tr>
                                                @foreach($stats[$filter['key_name']] as $stat)
                                                    <tr>
                                                        <td>{{ ucfirst($stat['description']) }}</td>
                                                        <td>{{ $stat['count'] }}</td>
                                                        <td>{{ $stat['sum'] }} &euro;</td>
                                                    </tr>
                                                @endforeach
                                            </table>
                                        </div>
                                    @endisset
                                @endforeach
                            @else
                                <h2>No bets were made yet!</h2>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
