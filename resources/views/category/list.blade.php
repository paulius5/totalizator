@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Categories List
                        <a class="btn btn-sm btn-primary" href="{{ route('category.create') }}">{{ __('New') }}</a>
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <table class="table">
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Actions</th>
                            </tr>
                            @foreach($categories as $category)

                                    <tr>
                                        <td>{{ $category->id }}</td>
                                        <td>
                                            <a href="{{ route('category.show', [$category->id]) }}">
                                                {{ $category->name }}
                                            </a>
                                        </td>
                                        <td>{{ $category->description }}</td>
                                        <td>
                                            <a class='btn btn-sm btn-info' href="{{ route('category.show', [$category->id]) }}">{{ __('View') }}</a>
                                            <a class='btn btn-sm btn-success' href="{{ route('category.edit', [$category->id]) }}">{{ __('Edit') }}</a>
                                            <form action="{{ route('category.destroy', [$category->id]) }}" method="post" class="d-inline-block" onsubmit="return confirm('Do you really want to delete?');">
                                                @method('delete')
                                                @csrf
                                                <input class="btn btn-sm btn-danger" type="submit" value="Delete">
                                            </form>
                                        </td>
                                    </tr>

                            @endforeach
                        </table>
                        {{ $categories->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
