@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Category New
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form action="{{ route('category.store') }}" method="post">

                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="name">{{ __('Name') }}:</label>
                                <input id="name" class="form-control" type="text" name="name"
                                       value="{{ old('name') }}">
                                @if($errors->has('name'))
                                    <div class="alert-danger">{{ $errors->first('name') }}</div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="description">{{ __('Description') }}:</label>
                                <input id="description" class="form-control" type="text" name="description"
                                       value="{{ old('description') }}">
                                @if($errors->has('description'))
                                    <div class="alert-danger">{{ $errors->first('description') }}</div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="score_from">{{ __('Score from') }}:</label>
                                <input id="score_from" class="form-control" type="number" name="score_from"
                                       value="{{ old('score_from') }}">
                                @if($errors->has('score_from'))
                                    <div class="alert-danger">{{ $errors->first('score_from') }}</div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="score_to">{{ __('Score to') }}:</label>
                                <input id="score_to" class="form-control" type="number" name="score_to"
                                       value="{{ old('score_to') }}">
                                @if($errors->has('score_to'))
                                    <div class="alert-danger">{{ $errors->first('score_to') }}</div>
                                @endif
                            </div>

                            <div class="form-group">
                                <input class="btn btn-success btn-sm" type="submit" value="{{ __('Save') }}">
                                <a class='btn btn-secondary btn-sm' href={{ route('category.index') }}>Back to list</a>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection