@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Event View: {{ $event->team1->name }} - {{ $event->team2->name }}
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <table class="table">
                            <tr>
                                <td>{{ __('ID') }}</td>
                                <td>{{ $event->id }}</td>
                            </tr>
                            <tr>
                                <td>{{ __('Category') }}</td>
                                <td>{{ $event->team1->subcategory->category->name }}</td>
                            </tr>
                            <tr>
                                <td>{{ __('Subcategory') }}</td>
                                <td>{{ $event->team1->subcategory->name }}</td>
                            </tr>
                            <tr>
                                <td>{{ __('Team1') }}</td>
                                <td>{{ $event->team1->name }}</td>
                            </tr>
                            <tr>
                                <td>{{ __('Team2') }}</td>
                                <td>{{ $event->team2->name }}</td>
                            </tr>
                            <tr>
                                <td>{{ $event->team1->name }} {{ __(' win rate') }}</td>
                                <td>{{ $event->team1_win_rate }}</td>
                            </tr>
                            <tr>
                                <td>{{ $event->team2->name }}{{ __(' win rate') }}</td>
                                <td>{{ $event->team2_win_rate }}</td>
                            </tr>
                            <tr>
                                <td>{{ __('Draw rate') }}</td>
                                <td>{{ $event->draw_rate }}</td>
                            </tr>
                            <tr>
                                <td>{{ __('Open bet at') }}</td>
                                <td>{{ $event->open_bet_at }}</td>
                            </tr>
                            <tr>
                                <td>{{ __('Close bet at') }}</td>
                                <td>{{ $event->close_bet_at }}</td>
                            </tr>
                            <tr>
                                <td>{{ __('Start game at') }}</td>
                                <td>{{ $event->start_game_at }}</td>
                            </tr>
                            <tr>
                                <td>{{ __('Finish game at') }}</td>
                                <td>{{ $event->finish_game_at }}</td>
                            </tr>
                            <tr>
                                <td>{{ __('Created at') }}</td>
                                <td>{{ $event->created_at }}</td>
                            </tr>
                            <tr>
                                <td>{{ __('Updated at') }}</td>
                                <td>{{ $event->updated_at }}</td>
                            </tr>
                        </table>

                        <a class='btn btn-secondary btn-sm' href={{ route('event.index') }}>Back to list</a>
                        <a class='btn btn-success btn-sm' href={{ route('event.edit', [$event['id']]) }}>Edit</a>
                        <form action="{{ route('event.destroy', [$event['id']]) }}" method="post" class="d-inline-block" onsubmit="return confirm('Do you really want to delete?');">
                            @method('delete')
                            @csrf
                            <input class="btn btn-danger btn-sm" type="submit" value="Delete">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection