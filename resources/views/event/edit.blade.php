@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Event Edit
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form action="{{ route('event.update', [$event['id']]) }}" method="post">

                            {{ method_field('put') }}
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="team1">{{ __('Team1') }}:</label>
                                {{ $event->team1->name }}
                            </div>
                            <div class="form-group">
                                <label for="team2">{{ __('Team2') }}:</label>
                                {{ $event->team2->name }}
                            </div>
                            <div class="form-group">
                                <label for="category">{{ __('Category') }}:</label>
                                {{ $event->team1->subcategory->category->name }}
                            </div>
                            <div class="form-group">
                                <label for="subcategory">{{ __('Subcategory') }}:</label>
                                {{ $event->team1->subcategory->name }}
                            </div>
                            <div class="form-group">
                                <label for="team1_win_rate">{{ $event->team1->name }}{{ __(' win rate') }}: </label>
                                {{ $event->team1_win_rate }}
                            </div>
                            <div class="form-group">
                                <label for="team2_win_rate">{{ $event->team2->name }}{{ __(' win rate') }}: </label>
                                {{ $event->team2_win_rate }}
                            </div>
                            <div class="form-group">
                                <label for="draw_rate">{{ __('Draw rate') }}: </label>
                                {{ $event->draw_rate }}
                            </div>
                            <div class="form-group">
                                <label for="open_bet_at">{{ __('Open bet at') }}:</label>
                                <input id="open_bet_at" class="form-control" type="datetime-local" name="open_bet_at"
                                       value="{{ str_replace(' ', 'T', old('open_bet_at', $event->open_bet_at)) }}">
                                @if($errors->has('open_bet_at'))
                                    <div class="alert-danger">{{ $errors->first('open_bet_at') }}</div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="close_bet_at">{{ __('Close bet at') }}:</label>
                                <input id="close_bet_at" class="form-control" type="datetime-local" name="close_bet_at"
                                       value="{{ str_replace(' ', 'T', old('close_bet_at', $event->close_bet_at)) }}">
                                @if($errors->has('close_bet_at'))
                                    <div class="alert-danger">{{ $errors->first('close_bet_at') }}</div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="start_game_at">{{ __('Start game at') }}:</label>
                                <input id="start_game_at" class="form-control" type="datetime-local" name="start_game_at"
                                       value="{{ str_replace(' ', 'T', old('start_game_at', $event->start_game_at)) }}">
                                @if($errors->has('start_game_at'))
                                    <div class="alert-danger">{{ $errors->first('start_game_at') }}</div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="finish_game_at">{{ __('Finish game at') }}:</label>
                                <input id="finish_game_at" class="form-control" type="datetime-local" name="finish_game_at"
                                       value="{{ str_replace(' ', 'T', old('finish_game_at', $event->finish_game_at)) }}">
                                @if($errors->has('finish_game_at'))
                                    <div class="alert-danger">{{ $errors->first('finish_game_at') }}</div>
                                @endif
                            </div>
                            <div class="form-group">
                                <input class="btn btn-success btn-sm" type="submit" value="{{ __('Save') }}">
                                <a class='btn btn-secondary btn-sm' href={{ route('event.index') }}>Back to list</a>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection