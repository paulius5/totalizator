@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Events List
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <table class="table">
                            <tr>
                                <th>ID</th>
                                <th>Team1</th>
                                <th>Team2</th>
                                <th>Open bet at</th>
                                <th>Close bet at</th>
                                <th>Start game at</th>
                                <th>Finish game at</th>
                                <th>Actions</th>
                            </tr>
                            @foreach($events as $event)
                                <tr>
                                    <td>{{ $event->id }}</td>
                                    <td>{{ $event->team1->name }}</td>
                                    <td>{{ $event->team2->name }}</td>
                                    <td>{{ $event->open_bet_at }}</td>
                                    <td>{{ $event->close_bet_at }}</td>
                                    <td>{{ $event->start_game_at }}</td>
                                    <td>{{ $event->finish_game_at }}</td>
                                    <td>
                                        <a class='btn btn-sm btn-info'
                                           href="{{ route('event.show', [$event->id]) }}">{{ __('View') }}</a>
                                        <a class='btn btn-sm btn-success'
                                           href="{{ route('event.edit', [$event->id]) }}">{{ __('Edit') }}</a>
                                        <form action="{{ route('event.destroy', [$event->id]) }}" method="post"
                                              class="d-inline-block"
                                              onsubmit="return confirm('Do you really want to delete?');">
                                            @method('delete')
                                            @csrf
                                            <input class="btn btn-sm btn-danger" type="submit" value="Delete">
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        {{ $events->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
