@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Team View: {{ $team->Name }}
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <table class="table">
                            <tr>
                                <td>{{ __('ID') }}</td>
                                <td>{{ $team->id }}</td>
                            </tr>
                            <tr>
                                <td>{{ __('Name') }}</td>
                                <td>{{ $team->name }}</td>
                            </tr>
                            <tr>
                                <td>{{ __('Description') }}</td>
                                <td>{{ $team->description }}</td>
                            </tr>
                            <tr>
                                <td>{{ __('Category') }}</td>
                                <td>{{ $team->subcategory->category->name }}</td>
                            </tr>
                            <tr>
                                <td>{{ __('Subcategory') }}</td>
                                <td>{{ $team->subcategory->name }}</td>
                            </tr>
                            <tr>
                                <td>{{ __('Created at') }}</td>
                                <td>{{ $team->created_at }}</td>
                            </tr>
                            <tr>
                                <td>{{ __('Updated at') }}</td>
                                <td>{{ $team->updated_at }}</td>
                            </tr>
                        </table>

                        <a class='btn btn-secondary btn-sm' href={{ route('team.index') }}>Back to list</a>
                        <a class='btn btn-success btn-sm' href={{ route('team.edit', [$team->id]) }}>Edit</a>
                        <form action="{{ route('team.destroy', [$team->id]) }}" method="post" class="d-inline-block" onsubmit="return confirm('Do you really want to delete?');">
                            @method('delete')
                            @csrf
                            <input class="btn btn-danger btn-sm" type="submit" value="Delete">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection