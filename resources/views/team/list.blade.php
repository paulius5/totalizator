@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Teams List
                        <a class="btn btn-sm btn-primary" href="{{ route('team.create') }}">{{ __('New') }}</a>
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <table class="table">
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Category</th>
                                <th>Subcategory</th>
                                <th>Actions</th>
                            </tr>
                            @foreach($teams as $team)
                                <tr>
                                    <td>{{ $team->id }}</td>
                                    <td>
                                        <a href="{{ route('team.show', [$team->id]) }}">
                                            {{ $team->name }}
                                        </a>
                                    </td>
                                    <td>{{ $team->description }}</td>
                                    <td>{{ $team->subcategory->category->name }}</td>
                                    <td>{{ $team->subcategory->name }}</td>
                                    <td>
                                        <a class='btn btn-sm btn-info' href="{{ route('team.show', [$team->id]) }}">{{ __('View') }}</a>
                                        <a class='btn btn-sm btn-success' href="{{ route('team.edit', [$team->id]) }}">{{ __('Edit') }}</a>
                                        <form action="{{ route('team.destroy', [$team->id]) }}" method="post" class="d-inline-block" onsubmit="return confirm('Do you really want to delete?');">
                                            @method('delete')
                                            @csrf
                                            <input class="btn btn-sm btn-danger" type="submit" value="Delete">
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        {{ $teams->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
