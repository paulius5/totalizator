@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Team New
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form action="{{ route('team.update', [$team->id]) }}" method="post">
                            {{ method_field('put') }}
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="name">{{ __('Name') }}:</label>
                                <input id="name" class="form-control" type="text" name="name"
                                       value="{{ old('name', $team->name) }}">
                                @if($errors->has('name'))
                                    <div class="alert-danger">{{ $errors->first('name') }}</div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="description">{{ __('Description') }}:</label>
                                <input id="description" class="form-control" type="text" name="description"
                                       value="{{ old('description', $team->description) }}">
                                @if($errors->has('description'))
                                    <div class="alert-danger">{{ $errors->first('description') }}</div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="category_id">{{ __('Category') }}:</label>
                                <select name="category_id" id="category_id" class="form-control" onchange="createSubcategoriesOptions(this.value);">
                                    <option value="" selected disabled>Select category</option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}" {{ (old('category_id', $team->subcategory->category->id) == $category->id)? 'selected' : '' }}>
                                            {{ $category->name }}
                                        </option>
                                    @endforeach
                                </select>
                                @if($errors->has('category_id'))
                                    <div class="alert-danger">{{ $errors->first('category_id') }}</div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="subcategory_id">{{ __('Subcategory') }}:</label>
                                <select name="subcategory_id" id="subcategory_id" class="form-control">
                                    <option disabled selected>Select subcategory</option>
                                </select>
                                @if($errors->has('subcategory_id'))
                                    <div class="alert-danger">{{ $errors->first('subcategory_id') }}</div>
                                @endif
                            </div>

                            <div class="form-group">
                                <input class="btn btn-success btn-sm" type="submit" value="{{ __('Save') }}">
                                <a class='btn btn-secondary btn-sm' href={{ route('team.index') }}>Back to list</a>
                            </div>

                        </form>
                        <script>
                            document.getElementById('category_id').addEventListener("load", createSubcategoriesOptions());
                            function createSubcategoriesOptions(id = null) {
                                if (id === null) {
                                    id = document.getElementById('category_id').value;
                                }
                                let subcategoriesArray =  JSON.parse('{!! $subcategories !!}');
                                document.getElementById('subcategory_id').innerHTML = '<option disabled selected>Select subcategory</option>';
                                subcategoriesArray.forEach(function(element) {
                                    if (element.category_id == id){
                                        let option = document.createElement("option");
                                        option.value = element.id;
                                        option.text = element.name;
                                        if (element.id == {!! old('subcategory_id', $team->subcategory_id) !!}) {
                                            option.defaultSelected = true;
                                        }
                                        document.getElementById('subcategory_id').appendChild(option);
                                    }
                                });
                            }
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection