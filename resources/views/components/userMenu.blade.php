<li class="nav-item dropdown">
    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
        {{ Auth::user()->name }} <span class="caret"></span>
    </a>

    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
        @player
        @else
            <a class="dropdown-item" href="{{ route('request') }}">
                {{ __('Request to bet') }}
            </a>
        @endplayer

        <a class="dropdown-item" href="{{ route('info') }}">
            {{ __('Info') }}
        </a>
        <a class="dropdown-item" href="{{ route('balance') }}">
            {{ __('Balance history') }}
        </a>
        <a class="dropdown-item" href="{{ route('logout') }}"
           onclick="event.preventDefault();
           document.getElementById('logout-form').submit();">
            {{ __('Logout') }}
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST"
              style="display: none;">
            @csrf
        </form>
    </div>
</li>