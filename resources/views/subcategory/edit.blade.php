@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Subcategory Edit
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form action="{{ route('subcategory.update', [$subcategory->id]) }}" method="post">

                            {{ method_field('put') }}
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="name">{{ __('Name') }}:</label>
                                <input id="name" class="form-control" type="text" name="name"
                                       value="{{ old('name', $subcategory->name) }}">
                                @if($errors->has('name'))
                                    <div class="alert-danger">{{ $errors->first('name') }}</div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="description">{{ __('Description') }}:</label>
                                <input id="description" class="form-control" type="text" name="description"
                                       value="{{ old('description', $subcategory->description) }}">
                                @if($errors->has('description'))
                                    <div class="alert-danger">{{ $errors->first('description') }}</div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="category_id">{{ __('Category') }}:</label>
                                <select name="category_id" id="category_id" class="form-control">
                                    <option value="" selected disabled>Select category</option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}" {{ (old('category_id', $subcategory->category->id) == $category->id)? 'selected' : '' }}>
                                            {{ $category->name }}
                                        </option>
                                    @endforeach
                                </select>
                                @if($errors->has('category_id'))
                                    <div class="alert-danger">{{ $errors->first('category_id') }}</div>
                                @endif
                            </div>

                            <div class="form-group">
                                <input class="btn btn-success btn-sm" type="submit" value="{{ __('Save') }}">
                                <a class='btn btn-secondary btn-sm' href={{ route('subcategory.index') }}>Back to list</a>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection