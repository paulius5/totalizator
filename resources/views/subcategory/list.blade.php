@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Subcategories List
                        <a class="btn btn-sm btn-primary" href="{{ route('subcategory.create') }}">{{ __('New') }}</a>
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <table class="table">
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Actions</th>
                            </tr>
                            @foreach($subcategories as $subcategory)
                                <tr>
                                    <td>{{ $subcategory->id }}</td>
                                    <td>
                                        <a href="{{ route('subcategory.show', [$subcategory->id]) }}">
                                            {{ $subcategory->name }}
                                        </a>
                                    </td>
                                    <td>{{ $subcategory->description }}</td>
                                    <td>
                                        <a class='btn btn-sm btn-info' href="{{ route('subcategory.show', [$subcategory->id]) }}">{{ __('View') }}</a>
                                        <a class='btn btn-sm btn-success' href="{{ route('subcategory.edit', [$subcategory->id]) }}">{{ __('Edit') }}</a>
                                        <form action="{{ route('subcategory.destroy', [$subcategory->id]) }}" method="post" class="d-inline-block" onsubmit="return confirm('Do you really want to delete?');">
                                            @method('delete')
                                            @csrf
                                            <input class="btn btn-sm btn-danger" type="submit" value="Delete">
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        {{ $subcategories->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
