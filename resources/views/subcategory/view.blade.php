@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Subcategory View: {{ $subcategory->Name }}
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <table class="table">
                            <tr>
                                <td>{{ __('ID') }}</td>
                                <td>{{ $subcategory->id }}</td>
                            </tr>
                            <tr>
                                <td>{{ __('Name') }}</td>
                                <td>{{ $subcategory->name }}</td>
                            </tr>
                            <tr>
                                <td>{{ __('Description') }}</td>
                                <td>{{ $subcategory->description }}</td>
                            </tr>
                            <tr>
                                <td>{{ __('Category') }}</td>
                                <td>{{ $subcategory->category->name }}</td>
                            </tr>
                            <tr>
                                <td>{{ __('Created at') }}</td>
                                <td>{{ $subcategory->created_at }}</td>
                            </tr>
                            <tr>
                                <td>{{ __('Updated at') }}</td>
                                <td>{{ $subcategory->updated_at }}</td>
                            </tr>
                        </table>

                        <a class='btn btn-secondary btn-sm' href={{ route('subcategory.index') }}>Back to list</a>
                        <a class='btn btn-success btn-sm' href={{ route('subcategory.edit', [$subcategory->id]) }}>Edit</a>
                        <form action="{{ route('subcategory.destroy', [$subcategory->id]) }}" method="post" class="d-inline-block" onsubmit="return confirm('Do you really want to delete?');">
                            @method('delete')
                            @csrf
                            <input class="btn btn-danger btn-sm" type="submit" value="Delete">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection