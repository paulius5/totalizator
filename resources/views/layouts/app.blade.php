<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    @routes

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
    @stack('styles')
</head>
<body>
<div id="app">
    <nav class="navbar navbar-expand-lg navbar-light navbar-laravel sticky-top">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                <img class="logo bounce animated" src="{{ asset('images/logo.png') }}">
                <span>𝕋𝕠𝕥𝕒𝕝𝕚𝕫𝕒𝕥𝕠𝕣</span>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @else
                        @if(canAccessAnyRoute([
                            'admin/role', 'admin/user',
                            'creator/category', 'creator/subcategory', 'creator/team', 'creator/event',
                            'company/bets', 'company/bets/open', 'company/bets/closed', 'company/bonus',
                        ], $userRoles))
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ __('Additional menu') }}<span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <!-- admin menu -->
                                    @if(canAccessAnyRoute(['admin/role', 'admin/user', 'admin/request'], $userRoles))
                                        <div class="menu-group">
                                            @if(canAccessRoute('admin/role', $userRoles))
                                                <a class="dropdown-item {{addClassOnActivePage('admin/role')}}" href="{{ url('admin/role') }}">
                                                    {{ __('Roles') }}
                                                </a>
                                            @endif

                                            @if(canAccessRoute('admin/user', $userRoles))
                                                <a class="dropdown-item {{addClassOnActivePage('admin/user')}}" href="{{ url('admin/user') }}">
                                                    {{ __('Users') }}
                                                </a>
                                            @endif
                                        </div>
                                    @endif

                                    <!-- Creator menu -->
                                    @if(canAccessAnyRoute(['creator/category', 'creator/subcategory', 'creator/team', 'creator/event'], $userRoles))
                                        <div class="menu-group">
                                            @if(canAccessRoute('creator/category', $userRoles))
                                                <a class="dropdown-item {{addClassOnActivePage('creator/category')}}" href="{{ url('creator/category') }}">
                                                    {{ __('Categories') }}
                                                </a>
                                            @endif

                                            @if(canAccessRoute('creator/subcategory', $userRoles))
                                                <a class="dropdown-item {{addClassOnActivePage('creator/subcategory')}}" href="{{ url('creator/subcategory') }}">
                                                    {{ __('Subcategories') }}
                                                </a>
                                            @endif

                                            @if(canAccessRoute('creator/team', $userRoles))
                                                <a class="dropdown-item {{addClassOnActivePage('creator/team')}}" href="{{ url('creator/team') }}">
                                                    {{ __('Teams') }}
                                                </a>
                                            @endif

                                            @if(canAccessRoute('creator/event', $userRoles))
                                                <a class="dropdown-item {{addClassOnActivePage('creator/event')}}" href="{{ url('creator/event') }}">
                                                    {{ __('Events') }}
                                                </a>
                                            @endif
                                        </div>
                                    @endif

                                    <!-- Company menu -->
                                    @if(canAccessAnyRoute(['company/bets', 'company/bets/open', 'company/bets/closed', 'company/bonus'], $userRoles))
                                        <div class="menu-group">
                                            @if(canAccessRoute('company/bets', $userRoles))
                                                <a class="dropdown-item {{addClassOnActivePage('company/bets')}}" href="{{ route('company.bets') }}">
                                                    {{ __('All bets') }}
                                                </a>
                                            @endif

                                            @if(canAccessRoute('company/bets/open', $userRoles))
                                                <a class="dropdown-item {{addClassOnActivePage('company/bets/open')}}" href="{{ route('company.open.bets') }}">
                                                    {{ __('All open bets') }}
                                                </a>
                                            @endif

                                            @if(canAccessRoute('company/bets/closed', $userRoles))
                                                <a class="dropdown-item {{addClassOnActivePage('company/bets/closed')}}" href="{{ route('company.closed.bets') }}">
                                                    {{ __('All closed bets') }}
                                                </a>
                                            @endif

                                            @if(canAccessRoute('company/bonus', $userRoles))
                                                <a class="dropdown-item {{addClassOnActivePage('company/bonus')}}" href="{{ url('company/bonus') }}">
                                                    {{ __('Bonuses') }}
                                                </a>
                                            @endif
                                        </div>
                                    @endif
                                </div>
                            </li>
                        @endif

                        @if(canAccessAnyRoute(['player/events/available', 'player/bets/open', 'player/bets/closed', 'player/statistic'], $userRoles))
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ __('Player menu') }}<span class="caret"></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <!-- Player menu -->
                                        <div class="menu-group">
                                            @if(canAccessRoute('player/events/available', $userRoles))
                                                <a class="dropdown-item {{addClassOnActivePage('player/events/available')}}" href="{{ route('player.available.events') }}">
                                                    {{ __('Available events') }}
                                                </a>
                                            @endif

                                            @if(canAccessRoute('player/bets/open', $userRoles))
                                                <a class="dropdown-item {{addClassOnActivePage('player/bets/open')}}" href="{{ route('player.open.bets') }}">
                                                    {{ __('My open bets') }}
                                                </a>
                                            @endif

                                            @if(canAccessRoute('player/bets/closed', $userRoles))
                                                <a class="dropdown-item {{addClassOnActivePage('player/bets/closed')}}" href="{{ route('player.closed.bets') }}">
                                                    {{ __('My closed bets') }}
                                                </a>
                                            @endif

                                            @if(canAccessRoute('player/statistic', $userRoles))
                                                <a class="dropdown-item {{addClassOnActivePage('player/statistic')}}" href="{{ route('player.statistic') }}">
                                                    {{ __('My statistic') }}
                                                </a>
                                            @endif
                                        </div>
                                </div>
                            </li>
                        @endif

                        <li class="nav-item">
                            <div class="form-inline">
                                <div class="form-inline">
                                    <a class="nav-link {{addClassOnActivePage('user/info')}} pr-3 pr-lg-1" href="{{ route('info') }}">
                                        {{ __('Personal info') }}
                                    </a>
                                    <a class="nav-link {{addClassOnActivePage('user/balance')}} pr-3 pr-lg-1" href="{{ route('balance') }}">
                                        {{ __('Balance history') }}
                                    </a>
                                    <a class="nav-link" href="{{ route('logout') }}"
                                         onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                          style="display: none;">
                                          @csrf
                                    </form>
                                </div>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>

    <main class="py-4">
        @yield('content')
    </main>
</div>
<div id="notification"></div>
<script>
    window.setTimeout(function() {
        $(".alert").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove();
        });
    }, 2000);
</script>
<footer>
    &copy; {{ date('Y') }} Totalizator <img src="{{ asset('../images/n-18.png') }}"  alt="N-18">
</footer>
</body>
</html>
