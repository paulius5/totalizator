@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Balance history (current balance is:
                        <strong>{{ $user->balance }}&euro;)</strong>
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <table class="table">
                            <tr>
                                <th>Amount</th>
                                <th>Balance before transaction</th>
                                <th>Message</th>
                                <th>Operation time</th>
                            </tr>

                            @foreach($balanceHistory as $balance)
                                <tr>
                                    <td class="font-weight-bold {{ ($balance->sign == '-')? 'text-danger' : 'text-success' }}">{{ $balance->sign }}{{ $balance->amount }}&euro;</td>
                                    <td>{{ $balance->balance }}&euro;</td>
                                    <td>{{ $balance->message }}</td>
                                    <td>{{ $balance->created_at }}</td>
                                </tr>
                            @endforeach

                        </table>
                        {{ $balanceHistory->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
