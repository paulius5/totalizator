@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header alert-warning">Page not found!</div>
                    <div class="card-body alert-info">
                        The page You entered, not found. Try again or try another one.
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('styles')
    <link href="{{ asset('css/error.css') }}" rel="stylesheet">
@endpush