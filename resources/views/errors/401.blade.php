@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header alert-warning">Insufficient permissions!</div>
                    <div class="card-body alert-info">
                        You don't have rights to visit this page.
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('styles')
    <link href="{{ asset('css/error.css') }}" rel="stylesheet">
@endpush