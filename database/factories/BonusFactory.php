<?php

use App\Bonus;
use Faker\Generator as Faker;

$factory->define(Bonus::class, function (Faker $faker) {
    return [
        'amount'        => random_int(100, 1000),
        'description'   => 'Initial bonus for registration',
        'active'        => 1,
    ];
});
