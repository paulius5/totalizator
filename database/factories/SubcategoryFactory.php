<?php

declare(strict_types = 1);

use App\Subcategory;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Subcategory::class, function (Faker $faker) {
    $name = $faker->name;
    $slug = Str::slug($name);
    return [
        'name'          => $name,
        'description'   => $faker->text,
        'category_id'   => $faker->unique(),
        'slug'          => $slug,
    ];
});
