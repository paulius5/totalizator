<?php

use App\Result;
use Faker\Generator as Faker;

$factory->define(Result::class, function (Faker $faker) {
    return [
        'event_id'      => random_int(1, 10),
        'team1_score'   => random_int(1, 100),
        'team2_score'   => random_int(1, 100),
    ];
});
