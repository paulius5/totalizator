<?php

use App\Bet;
use Faker\Generator as Faker;

$factory->define(Bet::class, function (Faker $faker) {
    return [
        'user_id'       => random_int(1, 100),
        'event_id'      => random_int(1, 100),
        'bet_for'       => random_int(0, 2),
        'bet_rate'      => $faker->randomFloat(2, 1, 10),
        'bet_amount'    => $faker->randomFloat(2, 1, 10),
        'closed'        => 0,
    ];
});
