<?php

declare(strict_types = 1);

use App\Category;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Category::class, function (Faker $faker) {
    $name = $faker->name;
    $slug = Str::slug($name);
    return [
        'name'          => $name,
        'description'   => $faker->text,
        'score_from'    => $faker->numberBetween(1, 10),
        'score_to'      => $faker->numberBetween(20, 50),
        'slug'          => $slug,
    ];
});
