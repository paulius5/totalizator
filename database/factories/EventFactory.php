<?php

use App\Event;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(Event::class, function (Faker $faker) {
    return [
        'team1_id'          => random_int(1, 100),
        'team2_id'          => random_int(101, 200),
        'open_bet_at'       => Carbon::now(),
        'close_bet_at'      => Carbon::now()->addMinutes(1),
        'start_game_at'     => Carbon::now()->addMinutes(2),
        'finish_game_at'    => Carbon::now()->addMinutes(3),
        'team1_win_rate'    => $faker->randomFloat(2, 1, 10),
        'team2_win_rate'    => $faker->randomFloat(2, 1, 10),
        'draw_rate'         => $faker->randomFloat(2, 1, 10),
    ];
});
