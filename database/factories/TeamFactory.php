<?php

declare(strict_types = 1);

use App\Team;
use Faker\Generator as Faker;

/** Illuminate/View/Factory $factory */
$factory->define(Team::class, function (Faker $faker) {
    return [
        'name'              => $faker->name,
        'description'       => $faker->text,
        'subcategory_id'    => $faker->unique(),
    ];
});
