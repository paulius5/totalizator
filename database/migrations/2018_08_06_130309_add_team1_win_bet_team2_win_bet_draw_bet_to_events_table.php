<?php

declare (strict_types=1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class AddTeam1WinBetTeam2WinBetDrawBetToEventsTable
 */
class AddTeam1WinBetTeam2WinBetDrawBetToEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        if (!Schema::hasColumn('events', 'team1_win_bet') && !Schema::hasColumn('events', 'team2_win_bet') && !Schema::hasColumn('events', 'draw_bet')) {
            Schema::table('events', function (Blueprint $table) {
                $table->double('team1_win_bet', 5, 2)->default(1);
                $table->double('team2_win_bet',5, 2)->default(1);
                $table->double('draw_bet',5, 2)->default(2);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('events', function (Blueprint $table) {
            //
        });
    }
}
