<?php

declare (strict_types=1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class ChangeColumnNamesOnEventsTable
 */
class ChangeColumnNamesOnEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        if (Schema::hasColumn('events', 'team1_win_bet')) {
            Schema::table('events', function (Blueprint $table) {
                $table->dropColumn(['team1_win_bet']);
            });
        }

        if (Schema::hasColumn('events', 'team2_win_bet')) {
            Schema::table('events', function (Blueprint $table) {
                $table->dropColumn(['team2_win_bet']);
            });
        }

        if (Schema::hasColumn('events', 'draw_bet')) {
            Schema::table('events', function (Blueprint $table) {
                $table->dropColumn(['draw_bet']);
            });
        }

        Schema::table('events', function (Blueprint $table) {
            $table->double('team1_win_rate', 5, 2)->default(1.5);
            $table->double('team2_win_rate', 5, 2)->default(1.5);
            $table->double('draw_rate', 5, 2)->default(2.5);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('event', function (Blueprint $table) {
            //
        });
    }
}
