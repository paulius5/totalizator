<?php

declare (strict_types=1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMessageColumnOnBalanceHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        if (!Schema::hasColumn('balance_history', 'message')) {
            Schema::table('balance_history', function (Blueprint $table) {
                $table->string('message', 191)->after('sign')->default('Bet placed.');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('balance_history', function (Blueprint $table) {
            //
        });
    }
}
