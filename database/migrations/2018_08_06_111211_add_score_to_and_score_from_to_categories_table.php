<?php

declare (strict_types=1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class AddScoreToAndScoreFromToCategoriesTable
 */
class AddScoreToAndScoreFromToCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        if (!Schema::hasColumn('categories', 'score_from') && !Schema::hasColumn('categories', 'score_to')) {
            Schema::table('categories', function (Blueprint $table) {
                $table->integer('score_from')->default(0);
                $table->integer('score_to')->default(0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('categories', function (Blueprint $table) {
            //
        });
    }
}
