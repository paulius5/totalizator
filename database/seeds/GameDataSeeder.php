<?php

declare(strict_types = 1);

use App\Category;
use App\Subcategory;
use App\Team;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

/**
 * Class GameDataSeeder
 */
class GameDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $categories = [
            [
                'name'          => 'Basketball',
                'description'   => 'Events related to basketball.',
                'score_from'    => 60,
                'score_to'      => 120,
            ],
            [
                'name'          => 'Football',
                'description'   => 'Events related to football.',
                'score_from'    => 0,
                'score_to'      => 10,
            ],
            [
                'name'          => 'Volleyball',
                'description'   => 'Events related to volleyball.',
                'score_from'    => 10,
                'score_to'      => 25,
            ],
            [
                'name'          => 'Handball',
                'description'   => 'Events related to handball.',
                'score_from'    => 10,
                'score_to'      => 50,
            ],
            [
                'name'          => 'Ice hockey',
                'description'   => 'Events related to ice hockey.',
                'score_from'    => 0,
                'score_to'      => 15,
            ],
            [
                'name'          => 'Snooker',
                'description'   => 'Events related to snooker.',
                'score_from'    => 0,
                'score_to'      => 10,
            ],
            [
                'name'          => 'Tennis',
                'description'   => 'Events related to tennis.',
                'score_from'    => 0,
                'score_to'      => 3,
            ],
        ];

        $subcategories = [
            'Basketball' => [
                [
                'name'          => 'Euroleague',
                'description'   => 'The strongest basketball league in Europe.'
                ],
                [
                'name'          => 'NBA',
                'description'   => 'The strongest basketball league in the world.'
                ],
            ],
            'Football' => [
                [
                    'name'          => 'La Liga',
                    'description'   => 'Spain football league.'
                ],
                [
                    'name'          => 'Bundesliga',
                    'description'   => 'Germany football league.'
                ],
                [
                    'name'          => 'Premier league',
                    'description'   => 'England football league.'
                ],
            ],
            'Volleyball' => [
                [
                    'name'          => 'A lyga',
                    'description'   => 'Highest level of men volleyball club tournament in Lithuania.'
                ],
                [
                    'name'          => 'Superleague',
                    'description'   => 'Russian volleyball league.'
                ],
            ],
            'Handball' => [
                [
                    'name'          => 'LRL',
                    'description'   => 'The strongest handball league in Lithuania.'
                ],
                [
                    'name'          => 'Meistriliiga',
                    'description'   => 'The name of the professional handball league of Estonia.'
                ],
            ],
            'Ice hockey' => [
                [
                    'name'          => 'NHL',
                    'description'   => 'National hockey league.'
                ],
                [
                    'name'          => 'Optibet Hockey League',
                    'description'   => 'Latvian hockey league.'
                ],
            ],
            'Snooker' => [
                [
                    'name'          => 'Premier League Snooker',
                    'description'   => 'England snooker league.'
                ],
                [
                    'name'          => 'Evergrande China',
                    'description'   => 'Snooker league in China.'
                ],
            ],
            'Tennis' => [
                [
                    'name'          => 'Wimbledon',
                    'description'   => 'Tournament in England.'
                ],
                [
                    'name'          => 'Australian Open',
                    'description'   => 'Tournament in Australia.'
                ],
                [
                    'name'          => 'Fed Cup',
                    'description'   => 'Women Tournament.'
                ],
                [
                    'name'          => 'Davis Cup',
                    'description'   => 'Men tournament.'
                ],
            ]
        ];

        $teams = [
            'Euroleague' => [
                [
                    'name'          => 'BC Žalgiris',
                    'description'   => 'Strongest Lithuanian basketball team.',
                ],
                [
                    'name'          => 'BC Real Madrid',
                    'description'   => 'Basketball club of Spain.',
                ],
                [
                    'name'          => 'BC CSKA',
                    'description'   => 'Basketball club of Russia.',
                ],
                [
                    'name'          => 'BC Olympiacos',
                    'description'   => 'Basketball club of Greece.',
                ],
                [
                    'name'          => 'BC Panathinaikos',
                    'description'   => 'Basketball club of Greece.',
                ],
                [
                    'name'          => 'BC Maccabi',
                    'description'   => 'Basketball club of Israel.',
                ],
                [
                    'name'          => 'BC Gran Canaria',
                    'description'   => 'Basketball club of Spain.',
                ],
                [
                    'name'          => 'BC Barcelona',
                    'description'   => 'Basketball club of Spain.',
                ],
                [
                    'name'          => 'BC AX Armani',
                    'description'   => 'Basketball club of Italy.',
                ],
            ],
            'NBA' => [
                [
                    'name'          => 'Toronto Raptors',
                    'description'   => 'Basketball club of Toronto city in USA.',
                ],
                [
                    'name'          => 'Boston Celtics',
                    'description'   => 'Basketball club of Boston city in USA.',
                ],
                [
                    'name'          => 'Orlando Magic',
                    'description'   => 'Basketball club of Orlando city in USA.',
                ],
                [
                    'name'          => 'Miami Heat',
                    'description'   => 'Basketball club of Miami city in USA.',
                ],
                [
                    'name'          => 'New York Knicks',
                    'description'   => 'Basketball club of New York city in USA.',
                ],
                [
                    'name'          => 'Chicago Bulls',
                    'description'   => 'Basketball club of Chicago city in USA.',
                ],
                [
                    'name'          => 'Cleveland Cavaliers',
                    'description'   => 'Basketball club of Cleveland city in USA.',
                ],
                [
                    'name'          => 'LA Clipers',
                    'description'   => 'Basketball club of LA city in USA.',
                ],
                [
                    'name'          => 'Los Angeles Lakers',
                    'description'   => 'Basketball club of LA city in USA.',
                ],
                [
                    'name'          => 'Oklahoma City Thunder',
                    'description'   => 'Basketball club of Oklahoma city in USA.',
                ],
                [
                    'name'          => 'Golden State Warriors',
                    'description'   => 'Basketball club of Oakland city in USA.',
                ],
                [
                    'name'          => 'Houston Rockets',
                    'description'   => 'Basketball club of Houston city in USA.',
                ],
                [
                    'name'          => 'Sacramento Kings',
                    'description'   => 'Basketball club of Sacramento city in USA.',
                ],
                [
                    'name'          => 'Sun Antonio Spurs',
                    'description'   => 'Basketball club of San Antonio city in USA.',
                ],
            ],
            'La Liga' => [
                [
                    'name'          => 'FC Real Madrid',
                    'description'   => 'Football club of Spain.',
                ],
                [
                    'name'          => 'FC Barcelona',
                    'description'   => 'Football club of Spain.',
                ],
                [
                    'name'          => 'FC Valencia',
                    'description'   => 'Football club of Spain.',
                ],
                [
                    'name'          => 'FC Getafe',
                    'description'   => 'Football club of Spain.',
                ],
                [
                    'name'          => 'FC Real Betis',
                    'description'   => 'Football club of Spain.',
                ],
                [
                    'name'          => 'FC Atletico Madrid',
                    'description'   => 'Football club of Spain.',
                ],
            ],
            'Bundesliga' => [
                [
                    'name'          => 'FC Bayern',
                    'description'   => 'Football club of Germany.',
                ],
                [
                    'name'          => 'FC Borussia',
                    'description'   => 'Football club of Germany.',
                ],
                [
                    'name'          => 'FC Schalke 04',
                    'description'   => 'Football club of Germany.',
                ],
                [
                    'name'          => 'FC Bayer',
                    'description'   => 'Football club of Germany.',
                ],
                [
                    'name'          => 'FC Fortuna',
                    'description'   => 'Football club of Germany.',
                ],
                [
                    'name'          => 'FC Werder',
                    'description'   => 'Football club of Germany.',
                ],
            ],
            'Premier league' =>[
                [
                    'name'          => 'FC Man City',
                    'description'   => 'Football club of England.',
                ],
                [
                    'name'          => 'FC Liverpool',
                    'description'   => 'Football club of England.',
                ],
                [
                    'name'          => 'FC Tottenham',
                    'description'   => 'Football club of England.',
                ],
                [
                    'name'          => 'FC Chelsea',
                    'description'   => 'Football club of England.',
                ],
                [
                    'name'          => 'FC Arsenal',
                    'description'   => 'Football club of England.',
                ],
                [
                    'name'          => 'FC Everton',
                    'description'   => 'Football club of England.',
                ],
                [
                    'name'          => 'FC Man United',
                    'description'   => 'Football club of England.',
                ],
                [
                    'name'          => 'FC Leicester City',
                    'description'   => 'Football club of England.',
                ],
            ],
            'A lyga' => [
                [
                    'name'          => 'Etovis',
                    'description'   => 'Volleyball club of Kelmė.',
                ],
                [
                    'name'          => 'Amber Queen',
                    'description'   => 'Volleyball club of Klaipėda.',
                ],
                [
                    'name'          => 'RIO - startas',
                    'description'   => 'Volleyball club of Kaunas.',
                ],
                [
                    'name'          => 'Sūduva',
                    'description'   => 'Volleyball club of Marijampolė.',
                ],
            ],
            'Superleague' => [
                [
                    'name'          => 'Zenit Kazan',
                    'description'   => 'Volleyball club of Russia.',
                ],
                [
                    'name'          => 'Novosibirsk',
                    'description'   => 'Volleyball club of Russia.',
                ],
                [
                    'name'          => 'Novyi Urengoy',
                    'description'   => 'Volleyball club of Russia.',
                ],
                [
                    'name'          => 'Belgorod',
                    'description'   => 'Volleyball club of Russia.',
                ],
                [
                    'name'          => 'Kemerovo',
                    'description'   => 'Volleyball club of Russia.',
                ],
                [
                    'name'          => 'Ufa',
                    'description'   => 'Volleyball club of Russia.',
                ],
            ],
            'LRL' => [
                [
                    'name'          => 'Varsa - Stronglasas',
                    'description'   => 'Handball club of Alytus.',
                ],
                [
                    'name'          => 'Granitas',
                    'description'   => 'Handball club of Kaunas.',
                ],
                [
                    'name'          => 'Dragūnas',
                    'description'   => 'Handball club of Klaipėda.',
                ],
                [
                    'name'          => 'Ūla',
                    'description'   => 'Handball club of Varėna.',
                ],
                [
                    'name'          => 'HC Utena',
                    'description'   => 'Handball club of Utena.',
                ],
                [
                    'name'          => 'HC Vilnius',
                    'description'   => 'Handball club of Vilnius.',
                ],
                [
                    'name'          => 'VHC Šviesa',
                    'description'   => 'Handball club of Vilnius.',
                ],
            ],
            'Meistriliiga' => [
                [
                    'name'          => 'Nomme Kalju',
                    'description'   => 'Handball club of Estonia.',
                ],
                [
                    'name'          => 'Levadia',
                    'description'   => 'Handball club of Estonia.',
                ],
                [
                    'name'          => 'Flora',
                    'description'   => 'Handball club of Estonia.',
                ],
                [
                    'name'          => 'Trans',
                    'description'   => 'Handball club of Estonia.',
                ],
                [
                    'name'          => 'JK Tammeka',
                    'description'   => 'Handball club of Estonia.',
                ],
                [
                    'name'          => 'JK Vaprus',
                    'description'   => 'Handball club of Estonia.',
                ],
            ],
            'NHL' => [
                [
                    'name'          => 'Capitals',
                    'description'   => 'Ice hockey club of Washington.',
                ],
                [
                    'name'          => 'Islanders',
                    'description'   => 'Ice hockey club of New York.',
                ],
                [
                    'name'          => 'Maple Leafs',
                    'description'   => 'Ice hockey club of Toronto.',
                ],
                [
                    'name'          => 'Bruins',
                    'description'   => 'Ice hockey club of Boston.',
                ],
                [
                    'name'          => 'Canadiens',
                    'description'   => 'Ice hockey club of Motreal.',
                ],
                [
                    'name'          => 'Rangers',
                    'description'   => 'Ice hockey club of New York.',
                ],
                [
                    'name'          => 'Red Wings',
                    'description'   => 'Ice hockey club of Detroit.',
                ],
                [
                    'name'          => 'Predators',
                    'description'   => 'Ice hockey club of Nashville.',
                ],
                [
                    'name'          => 'Sharks',
                    'description'   => 'Ice hockey club of San Jose.',
                ],
            ],
            'Optibet Hockey League' => [
                [
                    'name'          => 'Riga Prizma',
                    'description'   => 'Ice hockey club of Latvia.',
                ],
                [
                    'name'          => 'Mogo',
                    'description'   => 'Ice hockey club of Latvia.',
                ],
                [
                    'name'          => 'Kurbads',
                    'description'   => 'Ice hockey club of Latvia.',
                ],
                [
                    'name'          => 'Zemgale',
                    'description'   => 'Ice hockey club of Latvia.',
                ],
                [
                    'name'          => 'Liepaja',
                    'description'   => 'Ice hockey club of Latvia.',
                ],
            ],
            'Premier League Snooker' => [
                [
                    'name'          => 'Ronnie O Sullivan',
                    'description'   => 'One of the best snooker player.',
                ],
                [
                    'name'          => 'Mark Selby',
                    'description'   => 'One of the best snooker player.',
                ],
                [
                    'name'          => 'John Higgins',
                    'description'   => 'One of the best snooker player.',
                ],
                [
                    'name'          => 'Alex Higgins',
                    'description'   => 'One of the best snooker player.',
                ],
                [
                    'name'          => 'Stephen Hendry',
                    'description'   => 'One of the best snooker player.',
                ],
                [
                    'name'          => 'Mark Williams',
                    'description'   => 'One of the best snooker player.',
                ],
                [
                    'name'          => 'Judd Trump',
                    'description'   => 'One of the best snooker player.',
                ],
                [
                    'name'          => 'Barry Hawkins',
                    'description'   => 'One of the best snooker player.',
                ],
                [
                    'name'          => 'Neil Robertson',
                    'description'   => 'One of the best snooker player.',
                ],
                [
                    'name'          => 'Stephen Maguire',
                    'description'   => 'One of the best snooker player.',
                ],
            ],
            'Evergrande China' => [
                [
                    'name'          => 'Marco Fu',
                    'description'   => 'Snooker player of China.',
                ],
                [
                    'name'          => 'Ding Junhui',
                    'description'   => 'Snooker player of China.',
                ],
                [
                    'name'          => 'Xiao Guodong',
                    'description'   => 'Snooker player of China.',
                ],
                [
                    'name'          => 'Liang Wenbo',
                    'description'   => 'Snooker player of China.',
                ],
                [
                    'name'          => 'Zhou Yuelong',
                    'description'   => 'Snooker player of China.',
                ],
                [
                    'name'          => 'Li Hang',
                    'description'   => 'Snooker player of China.',
                ],
                [
                    'name'          => 'Lyu Haotian',
                    'description'   => 'Snooker player of China.',
                ],
            ],
            'Wimbledon' => [
                [
                    'name'          => 'Novak Djokovic',
                    'description'   => 'Tennis player.',
                ],
                [
                    'name'          => 'Rafael Nadal',
                    'description'   => 'Tennis player.',
                ],
                [
                    'name'          => 'Roger Federer',
                    'description'   => 'Tennis player.',
                ],
                [
                    'name'          => 'Alexander Zverev',
                    'description'   => 'Tennis player.',
                ],
                [
                    'name'          => 'Juan Martin Del Potro',
                    'description'   => 'Tennis player.',
                ],
            ],
            'Australian Open' => [
                [
                    'name'          => 'John Isner',
                    'description'   => 'Tennis player.',
                ],
                [
                    'name'          => 'Ričardas Berankis',
                    'description'   => 'Tennis player.',
                ],
                [
                    'name'          => 'Laurynas Grigelis',
                    'description'   => 'Tennis player.',
                ],
                [
                    'name'          => 'Grigor Dimitrov',
                    'description'   => 'Tennis player.',
                ],
                [
                    'name'          => 'Ernests Gulbis',
                    'description'   => 'Tennis player.',
                ],
            ],
            'Davis Cup' => [
                [
                    'name'          => 'Radu Albot',
                    'description'   => 'Tennis player.',
                ],
                [
                    'name'          => 'Reilly Opelka',
                    'description'   => 'Tennis player.',
                ],
                [
                    'name'          => 'Evgeny Donskoy',
                    'description'   => 'Tennis player.',
                ],
                [
                    'name'          => 'Marcel Granollers',
                    'description'   => 'Tennis player.',
                ],
                [
                    'name'          => 'Filip Krajinovic',
                    'description'   => 'Tennis player.',
                ],
                [
                    'name'          => 'Laslo Djere',
                    'description'   => 'Tennis player.',
                ],
                [
                    'name'          => 'Denis Istomin',
                    'description'   => 'Tennis player.',
                ],
            ],
            'Fed Cup' => [
                [
                    'name'          => 'Simona Halep',
                    'description'   => 'Tennis player.',
                ],
                [
                    'name'          => 'Angelique Kerber',
                    'description'   => 'Tennis player.',
                ],
                [
                    'name'          => 'Caroline Wozniacki',
                    'description'   => 'Tennis player.',
                ],
                [
                    'name'          => 'Elina Svitolina',
                    'description'   => 'Tennis player.',
                ],
                [
                    'name'          => 'Naomi Osaka',
                    'description'   => 'Tennis player.',
                ],
                [
                    'name'          => 'Sloane Stephens',
                    'description'   => 'Tennis player.',
                ],
                [
                    'name'          => 'Petra Kvitova',
                    'description'   => 'Tennis player.',
                ],
                [
                    'name'          => 'Karolina Pliskova',
                    'description'   => 'Tennis player.',
                ],
                [
                    'name'          => 'Serena Williams',
                    'description'   => 'Tennis player.',
                ],
                [
                    'name'          => 'Venus Williams',
                    'description'   => 'Tennis player.',
                ],
            ]
        ];

        foreach ($categories as $category) {
            array_set($category, 'slug', Str::slug($category['name']));
            $categoryId = factory(Category::class)->create($category)->id;

            foreach ($subcategories[$category['name']] as $subcategory) {

                array_set($subcategory, 'category_id', $categoryId);
                array_set($subcategory, 'slug', Str::slug($subcategory['name']));
                $subcategoryId = factory(Subcategory::class)->create($subcategory)->id;

                foreach ($teams[$subcategory['name']] as $team) {

                    array_set($team, 'subcategory_id', $subcategoryId);
                    factory(Team::class)->create($team);
                }
            }
        }
    }
}
