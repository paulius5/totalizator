<?php

use App\Permission;
use App\Role;
use Illuminate\Database\Seeder;

/**
 * Class RolesSeeder
 */
class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $roles = [
            [
                'name'          => 'SuperAdmin',
                'description'   => 'Full application control.',
                'full_access'   => 1,
                'active'        => 1,
            ],
            [
                'name'          => 'Admin',
                'description'   => 'Take control of users.',
                'active'        => 1,
            ],
            [
                'name'          => 'Creator',
                'description'   => 'Can create categories, subcategories, teams.',
                'active'        => 1,
            ],
            [
                'name'          => 'Company',
                'description'   => 'Take control of bets and events.',
                'active'        => 1,
            ],
            [
                'name'          => 'Player',
                'description'   => 'Can make bets for events.',
                'default'       => 1,
                'active'        => 1,
            ],
        ];

        foreach ($roles as $role) {
            if (isset($role['full_access'])) {
                factory(Role::class)->create($role);
            } else {
                $permissions = Permission::where('route', 'LIKE', strtolower($role['name']) . '%')->get();

                $role = factory(Role::class)->create($role);
                $role->permissions()->sync($permissions);
            }
        }
    }
}
