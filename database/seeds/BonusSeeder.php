<?php

declare(strict_types = 1);

use App\Bonus;
use Illuminate\Database\Seeder;

/**
 * Class BonusSeeder
 */
class BonusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        factory(Bonus::class)->create();
    }
}
