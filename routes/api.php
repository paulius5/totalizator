<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/availableEvents', 'API\\EventController@index')->name('api.available.events');

Route::get('/categories', 'API\\CategoryController@index')->name('api.categories');

Route::post('/bet/place/{bet}', 'API\\BetController@place')->name('api.place.bet');