<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/session/user', function () {
    return Auth::user();
})->name('session.user');

Route::get('/', 'WelcomeController@welcome')->name('welcome');
Route::get('/error', 'HomeController@error')->name('error');
Route::get('/forbidden', 'HomeController@forbidden')->name('forbidden');

Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    Route::get('/user/info', 'UserController@info')->name('info');
    Route::get('/user/balance', 'BalanceHistoryController@show')->name('balance');

    Route::group(['prefix' => 'admin'],function () {
        Route::resource('/role', 'RoleController')
            ->middleware('permission:admin role');

        Route::resource('/user', 'UserController')
            ->middleware('permission:admin user')
            ->except(['store', 'create']);
    });

    /*
     * Creator routes
     */
    Route::group(['prefix' => 'creator'], function () {
        Route::resource('/category', 'CategoryController')
            ->middleware('permission:creator category');

        Route::resource('/subcategory', 'SubcategoryController')
            ->middleware('permission:creator subcategory');

        Route::resource('/team', 'TeamController')
            ->middleware('permission:creator team');

        Route::resource('/event', 'EventController')
            ->middleware('permission:creator event')
            ->except(['create', 'store']);
    });

    /*
     * Company routes
     */
    Route::group(['prefix' => 'company'], function () {
        Route::get('/bets', 'BetController@index')
            ->middleware('permission:company all bets')
            ->name('company.bets');

        Route::get('/bets/open', 'BetController@companyOpenBets')
            ->middleware('permission:company open bets')
            ->name('company.open.bets');

        Route::get('/bets/closed', 'BetController@companyClosedBets')
            ->middleware('permission:company closed bets')
            ->name('company.closed.bets');

        Route::resource('/bonus', 'BonusController')
            ->middleware('permission:company bonus')
            ->only(['index', 'create', 'store']);

        Route::post('/bonus/activate/{bonus}', 'BonusController@activate')
            ->middleware('permission:company bonus activate')
            ->name('bonus.activate');

        Route::post('/bonus/deactivate}', 'BonusController@deactivate')
            ->middleware('permission:company bonus deactivate')
            ->name('bonus.deactivate');
    });

    /*
     *  Player routes
    */
    Route::group(['prefix' => 'player'], function () {
        Route::get('/events/available', 'EventController@showAvailableEvents')
            ->middleware('permission:player events and bets')
            ->name('player.available.events');

        Route::get('/bets/open', 'BetController@playerOpenBets')
            ->middleware('permission:player open bets')
            ->name('player.open.bets');

        Route::get('/bets/closed', 'BetController@playerClosedBets')
            ->middleware('permission:player closed bets')
            ->name('player.closed.bets');

        Route::get('/statistic', 'Statistic\PlayerController@index')
            ->middleware('permission:player statistic')
            ->name('player.statistic');
    });
});