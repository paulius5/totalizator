#Totalizator

It's a free betting game. 
Can bet on most populars sports in the world like football, basketball, handball and even more.

##Requirements

###Tools

- composer
- Git
- npm

###Server

- `PHP` >= **7.1.3**
- `OpenSSL` PHP Extension
- `PDO` PHP Extension
- `Mbstring` PHP Extension
- `Tokenizer` PHP Extension
- `XML` PHP Extension
- `Ctype` PHP Extension
- `JSON` PHP Extension
- `cURL` PHP Extension

##Instructions

###Installation

- Create `mysql database` and login credentials.
- Clone project:
```
$ git clone https://paulius5@bitbucket.org/paulius5/totalizator.git
```
- Go to project directory.
- Create `.env` file from `.env.example` file.
- Change `.env` file `CACHE_DRIVER` value into `redis`.
- Add your `database credentials` to `.env` file.
- Install composer:
```
$ composer install
```
- Install npm:
```
$ npm install
```
- Generate key for application:
```
$ php artisan key:generate
```
- Run database migrations:
```
$ php artisan migrate
```
- Seed database with initial data:
```
$ php artisan db:seed --class=InitialDataSeeder
```
- (optional) SuperAdmin or any other user can be created by command:
```
$ php artisan user:create
```
- Add `* * * * * cd /path-to-your-project && php artisan schedule:run >> /dev/null 2>&1` Cron entry to your server to start application scheduler.

###DEV

- If you don't have virtual server, you can run `php artisan serve` command to create virtual server.