<?php

declare (strict_types=1);

namespace Tests\Unit\Repositories;

use App\Bet;
use App\Event;
use App\Repositories\BetRepository;
use App\Result;
use Illuminate\Support\Collection;
use Tests\MemoryDatabaseMigrations;
use Tests\TestCase;

class BetRepositoryTest extends TestCase
{
    use MemoryDatabaseMigrations;

    /**
     * @test
     * @group bet
     * @group bet-repository
     */
    public function it_should_create_singleton_instance(): void
    {
        $this->assertInstanceOf(BetRepository::class, $this->getTestClassInstance());
        $this->assertSame($this->getTestClassInstance(), $this->getTestClassInstance());
    }

    /**
     * @test
     *
     * @throws \Exception
     */
    public function it_should_return_empty_collection_on_get_event_by_id_which_not_closed(): void
    {
        $event = factory(Event::class)->create();

        factory(Bet::class)->create([
            'event_id' => $event->id,
        ]);

        $this->assertEmpty($this->getTestClassInstance()->getBetsInformationWhichNotClosedByEventId($event->id));
    }

    /**
     * @test
     *
     * @throws \Exception
     */
    public function it_should_return_collection_on_get_event_by_id_which_not_closed(): void
    {
        $event = factory(Event::class)->create();

        $bet = factory(Bet::class)->create([
            'event_id' => $event->id,
        ]);

        $resultsObj = factory(Result::class)->create([
            'event_id' => $event->id,
        ]);

        $res = $bet->toArray();
        $res['team1_score'] = $resultsObj->team1_score;
        $res['team2_score'] = $resultsObj->team2_score;

        $result = $this->getTestClassInstance()->getBetsInformationWhichNotClosedByEventId($event->id);

        $this->assertInstanceOf(Collection::class, $result);
        $this->assertEquals(collect($result[0]), collect($res));
    }

    /**
     * @return BetRepository
     */
    private function getTestClassInstance(): BetRepository
    {
        return $this->app->make(BetRepository::class);
    }
}
